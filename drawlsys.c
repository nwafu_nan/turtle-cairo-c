/*--------------------------------------------------------------------------------
 * Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
 * All rights reserved.
 *
 * 文件名称：drawlsys.c
 * 文件标识：见配置管理计划书
 * 摘要：l-system分形字符串解析绘制实现文件。
 *
 * 当前版本：1.0.1
 * 作者：耿楠
 * 完成日期：2022年11月04日
 *
 * 取代版本：1.0
 * 原作者：耿楠
 * 完成日期：2020年12月03日
 ------------------------------------------------------------------------------------*/
#include "drawlsys.h"

/*-----------------------------------------------------------------------
// 名称: void draw_lsys(turtle *t, const char *s, double step,
//                      double left_angle, double right_angle,
//                      double line_width, double scale)
// 功能: 解析L-System字符串，并用turtle绘图库绘制结果图形
// 算法: 通过遍历字符串，对每个字符进行判断后调用turtle库函进行绘图
// 参数:
//       [turtle *t] ------------ 龟行系统对象指针
//       [const char *s] -------- L-System字符串指针，const限定不可更改
//       [double step] ---------- 乌龟前进步长
//       [double left_angle] ---- 左转角度(取正值)
//       [double right_angle] --- 右转角度(取正值)
//       [double line_width] ---- 线宽
//       [double scale] --------- 线宽缩放比例
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月15日
// 注意：需要提前准备turtle绘图环境
//-----------------------------------------------------------------------*/
void draw_lsys(turtle *t, const char *s, double step, double left_angle, double right_angle,
               double line_width, double scale)
{
    /* 字符串循环 */
    while(*s != '\0')
    {
        switch(*s)
        {
            case 'F': /* 前进1步，并绘制 */
                t->setlinewidth(t, line_width);
                t->forward(t, step);
                line_width *= scale;
                break;
            case 'f': /* 前进1步，不绘制 */
                t->up(t);
                t->forward(t, step);
                line_width *= scale;
                t->down(t);
                break;
            case '-': /* 左转 */
                t->left(t, left_angle);
                break;
            case '+': /* 右转 */
                t->right(t, right_angle);
                break;
            case '[': /* 压栈 */
                t->push(t);
                break;
            case ']': /* 出栈 */
                t->pop(t);
                line_width = t->getlinewidth(t);
                break;
            default:
                break;
        }
        s++;
    }
}
