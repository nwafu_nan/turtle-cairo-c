# 用于编译使用Cairo图形库绘图C代码的Makefile:
# 仅用于Linux，请确保已安装Cairo库。
# Ubuntu 22.04 x86_64下的安装命令为：
#   sudo apt install libcairo2-dev
# 如果Ubuntu版本较旧，请使用apt-get命令安装 

# 编译器
CC = gcc

# 编译参数
CC_FLAGS = -Wall -O0 --std=c99 -g

# 链接参数: 链接Cairo图形库的库文件
CAIRO_INC = -I /usr/include/cairo
CAIRO_LIBS = -L /usr/lib/x86_64-linux-gnu/
L_FLAGS = -lcairo -lm

# 可执行文件名称
TARGET = test

# 源文件
SOURCES = $(wildcard *.c)
OBJECTS = $(SOURCES:%.c=%.o)

# 构建所有内容
all: $(SOURCES) $(TARGET)
#	make clean

# 链接，生成可执行文件
$(TARGET): $(OBJECTS)
	$(CC) $(OBJECTS) $(CAIRO_LIBS) $(L_FLAGS) -o $@
	@echo "Linking completed!"

# 编译，生成目标文件
%.o: %.c
	$(CC) -c $(CC_FLAGS) $(CAIRO_INC) $< -o $@
	@echo "Successfully compiled "$<"."

# 清理生成的文件
clean:
	rm -f $(OBJECTS) $(TARGET)
	@echo "Cleanup completed!"
