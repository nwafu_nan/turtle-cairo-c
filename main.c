/*--------------------------------------------------------------------------------
 * Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
 * All rights reserved.
 *
 * 文件名称：main.c
 * 文件标识：见配置管理计划书
 * 摘要：龟行绘图协议类测试驱动文件。
 *
 * 当前版本：1.0.1
 * 作者：耿楠
 * 完成日期：2022年11月04日
 *
 * 取代版本：1.0
 * 原作者：耿楠
 * 完成日期：2020年12月03日
 ------------------------------------------------------------------------------------*/
#include "turtle.h"
#include "drawlsys.h"
#include "lsys.h"
#include <stdio.h>
#include <string.h>

extern const bit32_color_t color_sets[];

void test_normcoord(turtle *);
void test_arc(turtle *);
void test_lsys(turtle *);
void test_koch(turtle *);
void test_tree1(turtle *);
void test_tree2(turtle *);
void test_tree3(turtle *);
void test_tree4(turtle *);
void test_sierpinski(turtle *);
void test_heart(turtle *, double, double, char*);
void test_putstr(turtle *);
void test_heart_arrow(turtle *, double, double, unsigned int);
void test_rose(turtle *);
char *color_name_strcat(char *, const char *);
void test_color(turtle *);

int main (int argc, char *argv[])
{
    turtle t;/*, tw, tl, tc, tlsys;*/

    turtle_ctor(&t);
    t.init(&t, 700, 750, "01_rose.pdf", TURTLE_OUT_TYPE_PDF, TURTLE_CANVAS_PIXEL);

    test_heart(&t, -180.0, -350.0, "pale_violet_red");
    test_rose(&t); /* 测试玫瑰花绘制 */
    test_putstr(&t);
    test_heart(&t, 110.0, -260.0, "medium_violet_red");
    test_heart_arrow(&t, -300.0, -320.0, 0xFFFF0000);

    t.save(&t);
    turtle_dtor(&t);

    turtle_ctor(&t);
    t.init(&t, 300, 300, "02_arc_line.pdf", TURTLE_OUT_TYPE_PDF, TURTLE_CANVAS_NORM);
    test_normcoord(&t); /* 测试归一化坐标的绘制 */
    t.save(&t);
    turtle_dtor(&t);

    turtle_ctor(&t);
    t.init(&t, 500, 3140, "03_color_names_table.pdf", TURTLE_OUT_TYPE_PDF, TURTLE_CANVAS_PIXEL);
    test_color(&t);  /* 绘制颜色枚举常量和字符串名称对应的填充颜色块 */
    t.save(&t);
    turtle_dtor(&t);

    turtle_ctor(&t);
    t.init(&t, 500, 500, "04_lsys_intro.pdf", TURTLE_OUT_TYPE_PDF, TURTLE_CANVAS_PIXEL);
    test_lsys(&t);  /* 测试L-System字符串绘制 */
    t.logo(&t);    /* 测试Turtle海龟标志绘制 */
    t.save(&t);
    turtle_dtor(&t);

    turtle_ctor(&t);
    t.init(&t, 300, 300, "05_koch.pdf", TURTLE_OUT_TYPE_PDF, TURTLE_CANVAS_PIXEL);
    test_koch(&t);  /* 绘制颜色枚举常量和字符串名称对应的填充颜色块 */
    t.save(&t);
    turtle_dtor(&t);

    turtle_ctor(&t);
    t.init(&t, 600, 600, "06_tree_normal.pdf", TURTLE_OUT_TYPE_PDF, TURTLE_CANVAS_PIXEL);
    test_tree1(&t);  /* 绘制颜色枚举常量和字符串名称对应的填充颜色块 */
    t.save(&t);
    turtle_dtor(&t);

    turtle_ctor(&t);
    t.init(&t, 600, 600, "07_tree_reg1.pdf", TURTLE_OUT_TYPE_PDF, TURTLE_CANVAS_PIXEL);
    test_tree2(&t);  /* 绘制颜色枚举常量和字符串名称对应的填充颜色块 */
    t.save(&t);
    turtle_dtor(&t);

    turtle_ctor(&t);
    t.init(&t, 600, 600, "08_tree_reg2.pdf", TURTLE_OUT_TYPE_PDF, TURTLE_CANVAS_PIXEL);
    test_tree3(&t);  /* 绘制颜色枚举常量和字符串名称对应的填充颜色块 */
    t.save(&t);
    turtle_dtor(&t);

    turtle_ctor(&t);
    t.init(&t, 800, 800, "09_tree_random.pdf", TURTLE_OUT_TYPE_PDF, TURTLE_CANVAS_PIXEL);
    test_tree4(&t);  /* 绘制颜色枚举常量和字符串名称对应的填充颜色块 */
    t.save(&t);
    turtle_dtor(&t);

    turtle_ctor(&t);
    t.init(&t, 600, 600, "10_sierpinski.pdf", TURTLE_OUT_TYPE_PDF, TURTLE_CANVAS_PIXEL);
    test_sierpinski(&t);  /* 绘制颜色枚举常量和字符串名称对应的填充颜色块 */
    t.save(&t);
    turtle_dtor(&t);

    return 0;
}

void test_putstr(turtle *t)
{
    t->reset(t);
    t->up(t);
    t->to(t, -260.0, -160.0);
    t->seth(t, 60);
    t->penenum(t, TURTLE_COLOR_FOREST_GREEN);
    t->putstr(t, "I LOVE", 42.0, "Arial");

    t->reset(t);
    t->up(t);
    t->to(t, 190.0, 150.0);
    t->seth(t, 45);
    t->penenum(t, TURTLE_COLOR_INDIGO);
    t->putstr(t, "程序员!", 42.0, "SimSun");
    t->down(t);
}

char *color_name_strcat(char *s, const char *color_name)
{
    while(*s)
        s++;

    while(*color_name)
    {
        if(*color_name >= 'a' && *color_name <= 'z')
        {
            *s = *color_name - 32;
        }
        else
        {
            *s = *color_name;
        }
        s++;
        color_name++;
    }

    *s = '\0';

    return s;
}

void test_color(turtle *t)
{
    int i;
    char s[81] = "TURTLE_COLOR_";

    t->reset(t);
    t->up(t);
    t->to(t, -40.0, t->main_turtle.ypos - 15);
    t->putstr(t, "预置颜色查找表", 11.0, "Adobe Heiti Std");
    t->down(t);

    t->reset(t);
    t->up(t);
    t->to(t, -200.0, t->main_turtle.ypos - 32);
    t->putstr(t, "枚举常量(enum)", 11.0, "Adobe Heiti Std");
    t->down(t);

    t->reset(t);
    t->up(t);
    t->to(t, 25.0, t->main_turtle.ypos - 32);
    t->putstr(t, "字符串名称", 11.0, "Adobe Heiti Std");
    t->down(t);

    t->reset(t);
    t->up(t);
    t->to(t, 190.0, t->main_turtle.ypos - 32);
    t->putstr(t, "颜色", 11.0, "Adobe Heiti Std");
    t->down(t);

    t->reset(t);
    t->up(t);
    t->to(t, -230.0, t->main_turtle.ypos - 30);
    t->down(t);

    for(i = 0; i < TURTLE_COLOR_NUMS; i++)
    {
        strcpy(s, "TURTLE_COLOR_");
        color_name_strcat(s, color_sets[i].color_name);

        t->up(t);
        t->seth(t, -90.0);
        t->go(t, 22.0);
        t->seth(t, 0.0);
        t->putstr(t, s, 11.0, "Adobe Heiti Std");
        t->go(t, 250.0);
        t->putstr(t, color_sets[i].color_name, 11.0, "Adobe Heiti Std");
        t->go(t, 150.0);
        t->seth(t, 90.0);
        t->go(t, 12.0);
        t->seth(t, 0.0);
        t->down(t);
        t->fillname(t, color_sets[i].color_name);
        t->beginfill(t);
          t->fd(t, 60.0);
          t->rt(t, 90.0);
          t->fd(t, 15.0);
          t->rt(t, 90.0);
          t->fd(t, 60.0);
        t->endfill(t);
        t->up(t);
        t->rt(t, 90.0);
        t->fd(t, 15.0);
        t->seth(t, -90.0);
        t->go(t, 12.0);
        t->seth(t, 180.0);
        t->go(t, 400.0);
    }
}

void test_normcoord(turtle *t)
{
    /* 绘制坐标系 */
    /* x轴 */
    t->up(t);
    t->to(t, -1.0, 0.0);
    t->down(t);
    t->fd(t, 2);
    t->push(t);
    t->left(t, 15.0);
    t->bk(t, 0.04);
    t->pop(t);
    t->right(t, 15.0);
    t->bk(t, 0.04);
    /* y轴 */
    t->up(t);
    t->to(t, 0.0, -1.0);
    t->down(t);
    t->setheading(t, 90);
    t->fd(t, 2.0);
    t->push(t);
    t->left(t, 15.0);
    t->bk(t, 0.04);
    t->pop(t);
    t->right(t, 15.0);
    t->bk(t, 0.04);

    /* 复位 */
    t->reset(t);
    t->pensize(t, 0.008);

    /* 第1象限的圆弧直线连接 */
    t->up(t);
    t->to(t, 0.3, 0.3);
    t->down(t);
    t->setheading(t, 15);
    t->setpencolor(t, 0xFAFF0000);
    t->forward(t, 0.4);
    t->penname(t, "green");
    t->circle(t, 0.15, 205, 60);
    t->penenum(t, TURTLE_COLOR_BLUE);
    t->forward(t, 0.4);

    /* 第2象限的圆弧直线连接 */
    t->up(t);
    t->to(t, -0.8, 0.5);
    t->down(t);
    t->setheading(t, 15);
    t->setpencolor(t, 0xFAFF0000);
    t->forward(t, 0.4);
    t->penname(t, "green");
    t->circle(t, -0.15, 0, 0);
    t->penenum(t, TURTLE_COLOR_BLUE);
    t->forward(t, 0.4);

    /* 第3象限的圆弧直线连接 */
    t->up(t);
    t->to(t, -0.9, -0.4);
    t->down(t);
    t->setheading(t, 15);
    t->setpencolor(t, 0xFAFF0000);
    t->forward(t, 0.4);
    t->penname(t, "green");
    t->circle(t, -0.15, -205, 60);
    t->penenum(t, TURTLE_COLOR_BLUE);
    t->forward(t, 0.4);

    /* 第4象限的圆弧直线连接 */
    t->up(t);
    t->to(t, 0.1, -0.6);
    t->down(t);
    t->setheading(t, 15);
    t->setpencolor(t, 0xFAFF0000);
    t->forward(t, 0.4);
    t->penname(t, "green");
    t->circle(t, 0.15, -205, 60);
    t->penenum(t, TURTLE_COLOR_BLUE);
    t->forward(t, 0.4);
}

void test_arc(turtle *t)
{
    t->reset(t);

    t->up(t);
    t->to(t, -220, -300);
    t->down(t);
    t->pensize(t, 15);
    t->setheading(t, 48);
    t->setpencolor(t, 0xFAFF0000);
    t->forward(t, 100);
    t->penname(t, "green");
    t->circle(t, 50, 205, 200);
    t->penenum(t, TURTLE_COLOR_BLUE);
    t->forward(t, 100);
}

/* 用L-system字符串绘制 */
void test_lsys(turtle *t)
{
    char *s1, *s2, *s3;

    s1 = "F-F+F+F-F-F-F+F+F-F+F-F+F+F-F+F-F+F+F-F-F-F+F+F-F-"
        "F-F+F+F-F-F-F+F+F-F+F-F+F+F-F+F-F+F+F-F-F-F+F+F-F+"
        "F-F+F+F-F-F-F+F+F-F+F-F+F+F-F+F-F+F+F-F-F-F+F+F-F+"
        "F-F+F+F-F-F-F+F+F-F+F-F+F+F-F+F-F+F+F-F-F-F+F+F-F-"
        "F-F+F+F-F-F-F+F+F-F+F-F+F+F-F+F-F+F+F-F-F-F+F+F-F"; /* Koch曲线 */
    s2 = "F-F++F-F++F-F++F-F++F-F";/* 五角星 */
    s3 = "F-F-F+F+F+F+F-F-F-F+F+F-F-F-F-F+F+F-F-F-F+F+F+F+F-F-"
         "F+F+F+F-F-F-F-F+F+F+F-F-F+F+F+F+F-F-F+F+F+F-F-F-F-F+"
         "F+F+F-F-F+F+F+F+F-F-F-F+F+F-F-F-F-F+F+F-F-F-F+F+F+F+"
         "F-F-F-F+F+F-F-F-F-F+F+F+F-F-F+F+F+F+F-F-F+F+F+F-F-F-"
         "F-F+F+F-F-F-F+F+F+F+F-F-F-F+F+F-F-F-F-F+F+F-F-F-F+F+"
         "F+F+F-F-F-F+F+F-F-F-F-F+F+F+F-F-F+F+F+F+F-F-F+F+F+F-"
         "F-F-F-F+F+F-F-F-F+F+F+F+F-F-F-F+F+F-F-F-F-F+F+F-F-F-"
         "F+F+F+F+F-F-F+F+F+F-F-F-F-F+F+F+F-F-F+F+F+F+F-F-F+F+"
         "F+F-F-F-F-F+F+F+F-F-F+F+F+F+F-F-F-F+F+F-F-F-F-F+F+F-"
         "F-F-F+F+F+F+F-F-F"; /* 谢宾斯基面处 */

    t->reset(t);
    t->up(t);
    t->to(t, -150, 80);
    t->down(t);
    draw_lsys(t, s1, 12, 90, 90, 1.0, 1.0);

    t->reset(t);
    t->up(t);
    t->to(t, -220, -90);
    t->down(t);
    t->penenum(t, TURTLE_COLOR_RED);
    t->fillenum(t, TURTLE_COLOR_RED);
    t->beginfill(t);
    draw_lsys(t, s2, 50, 72, 72, 1.0, 1.0);
    t->endfill(t);

    t->reset(t);
    t->up(t);
    t->to(t, -80, -230);
    t->down(t);
    t->penenum(t, TURTLE_COLOR_BROWN);
    draw_lsys(t, s3, 10, 60, 60, 1.0, 1.0);
}

void test_koch(turtle *t)
{
    sds lsys_str;

    /* 构造lsys */
    lsys_t l;
    lsys_ctor(&l);
    l.init(&l, "F--F--F", 0);
    l.setrules(&l, "F", "F+F--F+F");
    l.set_steps(&l, 5);
    lsys_str = sdsdup(l.gen(&l));
    lsys_dtor(&l);

    t->reset(t);
    t->up(t);
    t->seth(t, 90);
    t->to(t, 70, -115);
    t->down(t);
    t->penenum(t, TURTLE_COLOR_BROWN);
    draw_lsys(t, lsys_str, 1, 60, 60, 1.0, 1.0);

    sdsfree(lsys_str);
}
void test_tree1(turtle *t)
{
    sds lsys_str;

    /* 构造lsys */
    lsys_t l;
    lsys_ctor(&l);
    l.init(&l, "f", 0);
    l.app(&l, 'F', "FF");
    l.app(&l, 'f', "F-[[f]+f]+F[+Ff]-f");
    l.set_steps(&l, 6);
    lsys_str = sdsdup(l.gen(&l));
    lsys_dtor(&l);

    t->reset(t);
    t->up(t);
    t->seth(t, 90);
    t->to(t, 0, -280);
    t->down(t);
    t->penenum(t, TURTLE_COLOR_GREEN);
    draw_lsys(t, lsys_str, 3, 25, 25, 6.0, 0.98);

    sdsfree(lsys_str);
}

void test_tree2(turtle *t)
{
    sds lsys_str;

    /* 构造lsys */
    lsys_t l;
    lsys_ctor(&l);
    l.init(&l, "f", 0);
    l.setrules(&l, "Ff",  "FF", "F[+f][-f]Ff");
    l.set_steps(&l, 7);
    lsys_str = sdsdup(l.gen(&l));
    lsys_dtor(&l);

    t->reset(t);
    t->up(t);
    t->seth(t, 90);
    t->to(t, 0, -280);
    t->down(t);
    t->penenum(t, TURTLE_COLOR_GREEN);
    draw_lsys(t, lsys_str, 2, 30, 30, 2.0, 1.0);

    sdsfree(lsys_str);
}

void test_tree3(turtle *t)
{
    sds lsys_str;

    /* 构造lsys */
    lsys_t l;
    lsys_ctor(&l);
    l.init(&l, "f", 0);
    l.setrules(&l, "Ff",  "FF", "F[-f]+f");
    l.set_steps(&l, 9);
    lsys_str = sdsdup(l.gen(&l));
    lsys_dtor(&l);

    t->reset(t);
    t->up(t);
    t->seth(t, 90);
    t->to(t, 0, -280);
    t->down(t);
    t->penenum(t, TURTLE_COLOR_RED);
    draw_lsys(t, lsys_str, 1, 45, 45, 8.0, 0.99);

    sdsfree(lsys_str);
}

void test_tree4(turtle *t)
{
    sds lsys_str;

    /* 构造lsys */
    lsys_t l;
    lsys_ctor(&l);
    l.init(&l, "F", 1);
    l.app(&l, 'F', "F[+F]F[-F]F");
    l.app(&l, 'F', "F[+F]F[-F[+F]]");
    l.app(&l, 'F', "FF+[+F+F]-[+F]");
    l.set_steps(&l, 5);
    lsys_str = sdsdup(l.gen(&l));
    lsys_dtor(&l);

    t->reset(t);
    t->up(t);
    t->seth(t, 90);
    t->to(t, 0, -400);
    t->down(t);
    t->penenum(t, TURTLE_COLOR_BROWN);
    draw_lsys(t, lsys_str, 5.5, 25, 25, 2.0, 1.0);

    sdsfree(lsys_str);
}

void test_sierpinski(turtle *t)
{
    sds lsys_str;

    /* 构造lsys */
    lsys_t l;
    lsys_ctor(&l);
    l.init(&l, "F", 0);
    l.app(&l, 'F', "f-F-f");
    l.app(&l, 'f', "F+f+F");
    l.set_steps(&l, 9);
    lsys_str = sdsdup(l.gen(&l));
    lsys_dtor(&l);

    t->reset(t);
    t->up(t);
    t->to(t, -250, -200);
    t->down(t);
    t->penenum(t, TURTLE_COLOR_FUCHSIA);
    draw_lsys(t, lsys_str, 1.0, 60, 60, 1.0, 1.0);

    sdsfree(lsys_str);
}

void draw_heart_curve(turtle *t)
{
    int i;

    for(i = 0; i < 200; i++)
    {
        t->rt(t, 1.0);
        t->fd(t, 1.0);
    }
}
void test_heart(turtle *t, double x, double y, char *color)
{
    t->reset(t);

    t->up(t);
    t->to(t, x, y);
    t->down(t);
    t->pensize(t, 2);
    t->fillname(t, color);
    t->penname(t, "red");

    t->beginfill(t);
      t->lt(t, 140);
      t->fd(t, 113);
      draw_heart_curve(t);
      t->lt(t, 120);
      draw_heart_curve(t);
      t->fd(t, 112);
    t->endfill(t);
}

void test_heart_arrow(turtle *t, double x, double y, unsigned int color)
{
    t->reset(t);

    t->up(t);
    t->to(t, x, y);
    t->down(t);
    t->pensize(t, 15);
    t->seth(t, 22);
    t->penbit32(t, color);
    t->fd(t, 127.0);
    t->up(t);
    t->fd(t, 125.0);
    t->down(t);
    t->fd(t, 150.0);
    t->up(t);
    t->fd(t, 150.0);
    t->down(t);
    t->fd(t, 100.0);
    t->push(t);
    t->rt(t, 30);
    t->bk(t, 40);
    t->pop(t);
    t->lt(t, 30);
    t->bk(t, 40);
}

void test_rose(turtle *t)
{
    int i;
    double a;

    t->reset(t);

    t->up(t);
    t->to(t, 0, 280);  /* 玫瑰花的起始位置 */
    t->down(t);
    t->penname(t, "red");

    /* 花心内上1层*/
    t->left(t, 135);

    a = 0.2;
    for(i = 0; i < 80; i++)
    {
        if(i < 50)
        {
            t->setlinewidth(t, a);
            t->forward(t, 2);
            a += 0.15;
            if(i < 10)
            {
                t->left(t, 0.7);
            }
            else if(i >= 10 && i < 24)
            {
                t->left(t, 1);
            }
            else if(i >= 24 && i < 40)
            {
                t->left(t, 3);
            }
            else
            {
                t->left(t, 4);
            }
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 2.2);
            a -= 0.25;
            if(i >=50 && i < 65)
            {
                t->left(t, 6.7);
            }
            else
            {
                t->left(t, 2);
            }
        }
    }
    /* 花心内上2层*/
    t->setheading(t, 0);
    t->up(t);
    t->to(t, 40, 305);
    t->down(t);
    t->left(t, 145);
    a = 0.1;
    for(i = 0; i < 100; i++)
    {
        if(i < 60)
        {
            t->setlinewidth(t, a);
            t->forward(t, 3);
            a += 0.15;
            if(i < 10)
            {
                t->left(t, 0.8);
            }
            else if(i >= 10 && i < 24)
            {
                t->left(t, 0.8);
            }
            else if(i >= 24 && i < 40)
            {
                t->left(t, 1.7);
            }
            else
            {
                t->left(t, 2.2);
            }
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 2.4);
            a -= 0.15;
            if(i >=60 && i < 85)
            {
                t->left(t, 3.2);
            }
            else if(i >= 85 && i < 90)
            {
                t->left(t, 4);
            }
        }
    }
    /* 花心内右1层*/
    t->setheading(t, 0);
    t->up(t);
    t->to(t, 45, 320);
    t->down(t);
    t->left(t, 10);
    a = 0.1;
    for(i = 0; i < 100; i++)
    {
        if(i < 60)
        {
            t->setlinewidth(t, a);
            t->forward(t, 3);
            a += 0.15;
            if(i < 17)
            {
                t->right(t, 3.2);
            }
            else if(i >= 17 && i < 20)
            {
                t->right(t, 3.5);
            }
            else if(i >= 20 && i < 40)
            {
                t->right(t, 6.3);
            }
            else if(i >= 40 && i < 50)
            {
                t->right(t, 0.9);
            }
            else
            {
                t->left(t, 1);
            }
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 2.4);
            a -= 0.16;
            if(i >=60 && i < 85)
            {
                t->left(t, 0.4);
            }
            else if(i >= 85 && i < 90)
            {
                t->left(t, 1);
            }
        }
    }
    /* 花心内左3层*/
    t->setheading(t, 0);
    t->up(t);
    t->to(t, -135, 265);
    t->down(t);
    t->left(t, 160);
    a = 0.1;
    for(i = 0; i < 120; i++)
    {
        if(i < 60)
        {
            t->setlinewidth(t, a);
            t->forward(t, 3);
            a += 0.15;
            if(i < 10)
            {
                t->left(t, 2);
            }
            else if(i >= 10 && i < 15)
            {
                t->left(t, 8);
            }
            else if(i >= 15 && i < 35)
            {
                t->left(t, 6);
            }
            else if(i >= 35 && i < 40)
            {
                t->left(t, 2.8);
            }
            else
            {
                t->left(t, 1);
            }
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 3);
            a -= 0.15;
            if(i >=60 && i < 90)
            {
                t->right(t, 0.8);
            }
            else
            {
                t->right(t, 1.5);
            }
        }
    }
    /* 花心内右2层*/
    t->setheading(t, 0);
    t->up(t);
    t->to(t, 80, 250);
    t->down(t);
    t->left(t, 245);
    a = 0.2;
    for(i = 0; i < 80; i++)
    {
        if(i < 40)
        {
            t->setlinewidth(t, a);
            t->forward(t, 1.2);
            a += 0.15;
            if(i < 15)
            {
                t->left(t, 0.6);
            }
            else
            {
                t->left(t, 0.3);
            }
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 1.2);
            a -= 0.15;
            if(i >=40 && i < 65)
            {
                t->left(t, 0.6);
            }
            else
            {
                t->left(t, 0.35);
            }
        }
    }
    /* 花心内左4层*/
    t->setheading(t, 0);
    t->up(t);
    t->to(t, -150, 195);
    t->down(t);
    t->left(t, 300);
    a = 0.2;
    for(i = 0; i < 80; i++)
    {
        if(i < 40)
        {
            t->setlinewidth(t, a);
            t->forward(t, 1.2);
            a += 0.16;
            if(i < 15)
            {
                t->right(t, 0.6);
            }
            else
            {
                t->right(t, 0.3);
            }
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 1.2);
            a -= 0.16;
            if(i >=40 && i < 65)
            {
                t->right(t, 0.6);
            }
            else
            {
                t->right(t, 0.35);
            }
        }
    }
    /* 花心内左5层*/
    t->setheading(t, 0);
    t->up(t);
    t->to(t, -205, 225);
    t->down(t);
    t->left(t, 194);
    a = 0.1;
    for(i = 0; i < 100; i++)
    {
        if(i < 50)
        {
            t->setlinewidth(t, a);
            t->forward(t, 3);
            a += 0.15;
            if(i < 10)
            {
                t->left(t, 7);
            }
            else if(i >= 10 && i < 30)
            {
                t->left(t, 3);
            }
            else
            {
                t->left(t, 0.1);
            }
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 3);
            a -= 0.15;
            if(i >=50 && i < 80)
            {
                t->right(t, 0.3);
            }
            else
            {
                t->right(t, 0.6);
            }
        }
    }
    /* 花心内右3层*/
    t->setheading(t, 0);
    t->up(t);
    t->to(t, 110, 280);
    t->down(t);
    t->left(t, 6);
    a = 0.1;
    for(i = 0; i < 120; i++)
    {
        if(i < 60)
        {
            t->setlinewidth(t, a);
            t->forward(t, 3);
            a += 0.15;
            if(i < 10)
            {
                t->right(t, 4.5);
            }
            else if(i >= 10 && i < 30)
            {
                t->right(t, 5);
            }
            else
            {
                t->right(t, 0.1);
            }
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 3);
            a -= 0.15;
            if(i >=50 && i < 80)
            {
                t->left(t, 0.6);
            }
            else
            {
                t->left(t, 0.9);
            }
        }
    }
    /* 花心内左6层*/
    t->setheading(t, 0);
    t->up(t);
    t->to(t, -160, 110);
    t->down(t);
    t->left(t, 283);
    a = 0.2;
    for(i = 0; i < 80; i++)
    {
        if(i < 40)
        {
            t->setlinewidth(t, a);
            t->forward(t, 2.4);
            a += 0.15;
            if(i < 15)
            {
                t->right(t, 0.6);
            }
            else
            {
                t->right(t, 0.1);
            }
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 2.4);
            a -= 0.15;
            if(i >=40 && i < 65)
            {
                t->left(t, 2);
            }
            else
            {
                t->left(t, 3);
            }
        }
    }
    /* 花心内右4层*/
    t->setheading(t, 0);
    t->up(t);
    t->to(t, 90, 155);
    t->down(t);
    t->left(t, 258);
    a = 0.2;
    for(i = 0; i < 80; i++)
    {
        if(i < 40)
        {
            t->setlinewidth(t, a);
            t->forward(t, 3);
            a += 0.15;
            if(i < 10)
            {
                t->left(t, 0.8);
            }
            else
            {
                t->left(t, 0.1);
            }
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 3);
            a -= 0.15;
            if(i >=40 && i < 65)
            {
                t->right(t, 2);
            }
            else
            {
                t->right(t, 3);
            }
        }
    }
    /* 花心内正底1层*/
    t->setheading(t, 0);
    t->up(t);
    t->to(t, -60, -45);
    t->down(t);
    t->right(t, 9);
    a = 0.2;
    for(i = 0; i < 40; i++)
    {
        if(i < 20)
        {
            t->setlinewidth(t, a);
            t->forward(t, 1.3);
            a += 0.15;
            if(i < 5)
            {
                t->left(t, 0.7);
            }
            else
            {
                t->left(t, 0.4);
            }
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 1.3);
            a -= 0.15;
            if(i >=20 && i < 35)
            {
                t->left(t, 0.7);
            }
            else
            {
                t->left(t, 0.4);
            }
        }
    }
    /* 左绿叶上层*/
    t->setheading(t, 0);
    t->up(t);
    t->to(t, -70, -55);
    t->down(t);
    t->penrgb(t, 124, 252, 0);
    t->right(t, 170);
    a = 0.2;
    for(i = 0; i < 80; i++)
    {
        if(i < 40)
        {
            t->setlinewidth(t, a);
            t->forward(t, 2);
            a += 0.15;
            if(i < 25)
            {
                t->right(t, 0.5);
            }
            else
            {
                t->right(t, 1);
            }
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 2.5);
            a -= 0.15;
            if(i >=40 && i < 55)
            {
                t->right(t, 1);
            }
            else if(i >= 55 && i < 70)
            {
                t->right(t, 2);
            }
            else
            {
                t->right(t, 1);
            }
        }
    }
    /* 左绿叶下层*/
    t->setheading(t, 270);
    t->left(t, 30);
    a = 0.2;
    for(i = 0; i < 80; i++)
    {
        if(i < 40)
        {
            t->setlinewidth(t, a);
            t->forward(t, 2.5);
            a += 0.15;
            if(i < 25)
            {
                t->left(t, 0.5);
            }
            else
            {
                t->left(t, 1);
            }
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 3);
            a -= 0.15;
            if(i >=40 && i < 55)
            {
                t->left(t, 2);
            }
            else if(i >= 55 && i < 70)
            {
                t->left(t, 1.3);
            }
            else
            {
                t->left(t, 1);
            }
        }
    }
    /* 右绿叶上层*/
    t->setheading(t, 270);
    t->up(t);
    t->to(t, 20, -55);
    t->down(t);
    t->left(t, 65);
    a = 0.2;
    for(i = 0; i < 60; i++)
    {
        if(i < 30)
        {
            t->setlinewidth(t, a);
            t->forward(t, 2);
            a += 0.15;
            if(i < 15)
            {
                t->left(t, 1);
            }
            else
            {
                t->left(t, 2);
            }
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 2.5);
            a -= 0.15;
            if(i >=30 && i < 45)
            {
                t->left(t, 2);
            }
            else if(i >= 45 && i < 50)
            {
                t->left(t, 2.5);
            }
            else
            {
                t->left(t, 1.5);
            }
        }
    }
    /* 右绿叶下层*/
    t->setheading(t, 270);
    t->right(t, 17);
    a = 0.2;
    for(i = 0; i < 80; i++)
    {
        if(i < 40)
        {
            t->setlinewidth(t, a);
            t->forward(t, 2.2);
            a += 0.15;
            if(i < 25)
            {
                t->right(t, 1);
            }
            else
            {
                t->right(t, 2);
            }
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 2.5);
            a -= 0.15;
            if(i >=40 && i < 55)
            {
                t->right(t, 2);
            }
            else if(i >= 55 && i < 70)
            {
                t->right(t, 1.5);
            }
            else
            {
                t->right(t, 0.6);
            }
        }
    }
    /* 花柄左侧*/
    t->setheading(t, 0);
    t->up(t);
    t->to(t, -49, -90);
    t->down(t);
    t->penname(t, "brown");
    t->right(t, 89);
    a = 1;
    for(i = 0; i < 120; i++)
    {
        if(i < 60)
        {
            t->setlinewidth(t, a);
            t->forward(t, 3.5);
            a += 0.08;
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 3.5);
            a -= 0.08;
        }
    }
    /* 花柄左侧*/
    t->setheading(t, 0);
    t->up(t);
    t->to(t, -16, -70);
    t->down(t);
    t->right(t, 91);
    a = 1;
    for(i = 0; i < 120; i++)
    {
        if(i < 60)
        {
            t->setlinewidth(t, a);
            t->forward(t, 3.5);
            a += 0.08;
        }
        else
        {
            t->setlinewidth(t, a);
            t->forward(t, 3.5);
            a -= 0.08;
        }
    }
    /* 花柄右刺*/
    t->setheading(t, 0);
    t->up(t);
    t->to(t, -16, -150);
    t->down(t);
    t->setlinewidth(t, 3);
    t->left(t, 35);
    t->forward(t, 15);
    t->setheading(t, 0);
    t->right(t, 115);
    t->forward(t, 34);

    /* 花柄左刺*/
    t->setheading(t, 180);
    t->up(t);
    t->to(t, -49, -250);
    t->down(t);
    t->setlinewidth(t, 3);
    t->right(t, 35);
    t->forward(t, 15);
    t->setheading(t, 180);
    t->left(t, 115);
    t->forward(t, 34);
}
