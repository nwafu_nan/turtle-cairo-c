/*--------------------------------------------------------------------------------
 * Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
 * All rights reserved.
 *
 * 文件名称：lsys.c
 * 文件标识：见配置管理计划书
 * 摘要：L-System协议类封装实现文件。
 *
 * 当前版本：1.0
 * 作者：耿楠
 * 完成日期：2022年11月11日
 *
 * 取代版本：无
 * 原作者：
 * 完成日期：
 ------------------------------------------------------------------------------------*/
#include "lsys.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

/* 函数声明 */
static void lsys_init(void *, const char *, int);
static void lsys_app_rule(void *, char, const char *);
static void lsys_init_rules(void *, const char *, ...);
static void lsys_set_axiom(void *, const char *);
static void lsys_set_steps(void *, size_t);
static void lsys_set_random(void *, int);
static sds lsys_gen(void *);
static sds get_lsys(void *);
static void lsys_print(void*);

/* 内部函数 */
static lsys_t * _realloc_rules(lsys_t*, size_t);
static lsys_t * _realloc_rule_count(lsys_t*, size_t);
static int _lsys_cat(lsys_t*, char, sds, sds, sds (*)(lsys_t *, char));
static sds _get_normal_rule(lsys_t *, char);
static sds _get_random_rule(lsys_t *, char);
static void _sort_rules(lsys_t*);
int _qsort_cmp(const void*, const void*);
static size_t _create_rule_idx(lsys_t*);
static void _free_rules(lsys_t*);

/* 函数实现 */
/*-----------------------------------------------------------------------
// 名称: lsys_t * lsys_ctor(lsys_t* this)
// 功能: 构造lsys_t类型的L-System协议类对象
// 算法: 主要是对各函数指针按需进行赋值
// 参数:
//       [lsys_t* this] --- 协议类对象指针
// 返回: [lsys_t*] --- 该协议类对象的地址
// 作者: 耿楠
// 日期: 2022年11月11日
//-----------------------------------------------------------------------*/
lsys_t * lsys_ctor(lsys_t* this)
{
    this->init          = lsys_init;

    this->init_rules    = lsys_init_rules;
    this->setrules      = lsys_init_rules;

    this->app_rule      = lsys_app_rule;
    this->app           = lsys_app_rule;

    this->gen_lsys      = lsys_gen;
    this->gen           = lsys_gen;

    this->get_lsys      = get_lsys;

    this->set_axiom     = lsys_set_axiom;
    this->set_steps     = lsys_set_steps;
    this->set_random    = lsys_set_random;

    this->print         = lsys_print;

    return this;
}

/*-----------------------------------------------------------------------
// 名称: void lsys_dtor(lsys_t* this)
// 功能: 销毁lsys_t类型的L-System协议类对象
// 算法: 销毁所有申请的内存
// 参数:
//       [lsys_t* this] --- 协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月11日
//-----------------------------------------------------------------------*/
void lsys_dtor(lsys_t* this)
{
    int i;

    if(this->rules)
    {
        for(i = 0; i < this->rules_alloc; i++)
        {
            sdsfree(this->rules[i].rule);
        }
        free(this->rules);
    }

    sdsfree(this->axiom);
    sdsfree(this->lsys);

    if(this->rule_count)
    {
        free(this->rule_count);
    }
}

/*-----------------------------------------------------------------------
// 名称: static void lsys_init(void *this, const char *axiom, int random)
// 功能: 初始化lsys_t类型的L-System协议类对象
// 算法: 为各成员变量赋初值，并根据需要申请内存
// 参数:
//       [void* this] ---------- 协议类对象指针
//       [const char* axiom] --- L-System公理
//       [int random] ---------- 是否为随机L-System系统(0---非随机，非0---随机)
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月13日
// 备注: 添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static void lsys_init(void *this, const char *axiom, int random)
{
    /* 将this指针转换为Lsys指针 */
    lsys_t* This = (lsys_t*)this;

    This->axiom = sdsnew(axiom);

    This->rules_num = 0;
    This->rules_alloc = 0;

    This->lsys = NULL;
    This->steps = 0;

    This->rules = NULL;

    This->rule_count = NULL;
    /* 根据random值处理不同设置 */
    lsys_set_random(this, random);

    /* 置随机种子 */
    srand((unsigned)time(NULL));
}

/*-----------------------------------------------------------------------
// 名称: static void lsys_init_rules(void *this, const char *ch_sets, ...)
// 功能: 初始化L-System的替换规则
// 算法: 通过变长形参实现
// 参数:
//       [void* this] ------------ 协议类对象指针
//       [const char* ch_sets] --- 被替换字符组成的字符集
//       [...] ------------------- 变长形参，以逗号分隔的与ch_sets中
//                                 各字符对应的替换规则(字符串)
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月12日
// 备注: 添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static void lsys_init_rules(void *this, const char *ch_sets, ...)
{
    int i;
    va_list ap;
    char *s;
    size_t len;

    /* 将this指针转换为Lsys指针 */
    lsys_t* This = (lsys_t*)this;

    /* 初始化va_list对象 */
    va_start(ap, ch_sets);

    /* 清楚已有规则 */
    _free_rules(This);

    /* 计算规则数量 */
    len = strlen(ch_sets);
    This->rules_num = 0;
    This->rules_alloc = len;

    /* 重新申请规则结构体数组 */
    _realloc_rules(This, len);

    /* 处理每条规则 */
    for(i = 0; i < len; i++)
    {
        /* 取得变长形参中的对应字符串 */
        s = va_arg(ap, char *);

        This->rules[i].ch = ch_sets[i];
        This->rules[i].rule = sdsnew(s);
    }

    This->rules_num = len;

    /* 销毁变长形参对象 */
    va_end(ap);

    /* 重新申请规则索引结构体数组 */
    _realloc_rule_count(This, len);
}

/*-----------------------------------------------------------------------
// 名称: static void lsys_app_rule(void *this, char ch, const char *rule)
// 功能: 为L-System追加替换规则
// 参数:
//       [void* this] --------- 协议类对象指针
//       [char ch] ------------ 被替换字符
//       [const char* rule] --- 替换规则(字符串)
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月12日
// 备注: 添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static void lsys_app_rule(void *this, char ch, const char *rule)
{
    /* 将this指针转换为Lsys指针 */
    lsys_t* This = (lsys_t*)this;

    /* 如果规则结构体数组容量不足，则翻倍增长 */

    while(This->rules_num >= This->rules_alloc )
    {
        if(This->rules_alloc == 0)
        {
            This->rules_alloc = 1;
        }
        else
        {
            This->rules_alloc = 2 * This->rules_alloc;
        }
    }
    _realloc_rules(This, This->rules_alloc);

    This->rules[This->rules_num].ch = ch;
    This->rules[This->rules_num].rule = sdsnew(rule);

    This->rules_num++;

    /* 重新申请规则索引结构体数组 */
    _realloc_rule_count(This, This->rules_num);
}

/*-----------------------------------------------------------------------
// 名称: static sds lsys_gen(void *this)
// 功能: 生成L-System结果字符串
// 算法: 根据设定的参数生成非随机或随机L-System结果字符串
// 参数:
//       [void* this] --------- 协议类对象指针
// 返回: [sds] --- 生成的L-System结果字符串
//                 仅返回lsys_t结构体中的lsys字符串指针
//                 由于析构函数会释放该内存，不应直接通过该指针释放空间
//                 可以使用sdsdup函数复制该字符串
// 作者: 耿楠
// 日期: 2022年11月13日
// 备注: 添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static sds lsys_gen(void *this)
{
    ssize_t lsys_len;
    sds tmp1 = NULL;
    sds tmp2 = NULL;
    int i, k;
    char ch;

    /* 声明规则提取函数指针，将初始化为指向常规提取函数 */
    static sds (*pf_get_rule)(lsys_t *, char);
    pf_get_rule = _get_normal_rule;

    /* 将this指针转换为Lsys指针 */
    lsys_t* This = (lsys_t*)this;

    /* 初始化结果字符串为公理(从公理开始) */
    sdsfree(This->lsys);
    This->lsys = sdsdup(This->axiom);

    /* 如果是随机L-System，则首先对规则进行排版 */
    /* 然后建立索引数组，并让规则提取函数指针指向随机提取函数 */
    if(This->is_random)
    {
        _sort_rules(This);
        This->rule_idx_total = _create_rule_idx(This);
        _realloc_rule_count(This, This->rule_idx_total);
        pf_get_rule = _get_random_rule;
    }

    /* 迭代生成结果字符串 */
    for(i = 0; i < This->steps; i++)
    {
        k = 0;
        /* 取得当前结果字符串长度 */
        lsys_len = sdslen(This->lsys);
        ch = This->lsys[k];

        /* 处理第0个字母(无前导字符串) */
        /* 备份当前字符串以获取当前位置前后子字符串 */
        tmp1 = sdsdup(This->lsys);
        /* 取得当前位置后一个位置开始到结束的字符串 */
        sdsrange(tmp1, k + 1, -1);

        /* 完成替换 */
        k += _lsys_cat(This, ch, sdsempty(), tmp1, pf_get_rule);

        /* 更新长度 */
        lsys_len = sdslen(This->lsys);
        ch = This->lsys[k];

        /* 处理第1个字母到倒数第2个字母之间的字符 */
        while(k < lsys_len)
        {
            /* 备份当前字符串以获取当前位置前后子字符串 */
            tmp1 = sdsdup(This->lsys);
            tmp2 = sdsdup(This->lsys);

            /* 取得当前位置前后的子串 */
            sdsrange(tmp1, 0, k - 1);
            sdsrange(tmp2, k + 1, -1);

            /* 完成替换 */
            k += _lsys_cat(This, ch, tmp1, tmp2, pf_get_rule);

            /* 更新长度 */
            lsys_len = sdslen(This->lsys);
            ch = This->lsys[k];
        }

        /* 处理最后一个字符 */
        /* 备份当前字符串以获取当前位置前后子字符串 */
        tmp1 = sdsdup(This->lsys);

        /* 取得当前位置前的子串 */
        sdsrange(tmp1, 0, k - 1);

        /* 完成替换，不必再记录k值 */
        _lsys_cat(This, ch, sdsempty(), tmp1, pf_get_rule);
    }

    return This->lsys;
}

/*-----------------------------------------------------------------------
// 名称: static sds get_lsys(void *this)
// 功能: 返回生成的L-System结果字符串
// 参数:
//       [void* this] --------- 协议类对象指针
// 返回: [sds] --- 生成的L-System结果字符串
//                 仅返回lsys_t结构体中的lsys字符串指针
//                 由于析构函数会释放该内存，不应直接通过该指针释放空间
//                 可以使用sdsdup函数复制该字符串
// 作者: 耿楠
// 日期: 2022年11月13日
// 备注: 添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static sds get_lsys(void *this)
{
    /* 将this指针转换为Lsys指针 */
    lsys_t* This = (lsys_t*)this;

    return This->lsys;
}

/*-----------------------------------------------------------------------
// 名称: static void lsys_set_axiom(void *this, const char *axiom)
// 功能: 设置L-System公理
// 参数:
//       [void* this] ---------- 协议类对象指针
//       [const char *axiom] --- L-System公理(字符串)
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月13日
// 备注: 添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static void lsys_set_axiom(void *this, const char *axiom)
{
    /* 将this指针转换为Lsys指针 */
    lsys_t* This = (lsys_t*)this;

    sdsfree(This->axiom);
    This->axiom = sdsnew(axiom);
}

/*-----------------------------------------------------------------------
// 名称: static void lsys_set_steps(void *this, size_t steps)
// 功能: 设置L-System替换迭代次数
// 参数:
//       [void* this] ---------- 协议类对象指针
//       [size_t steps] --- L-System替换迭代次数
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月11日
// 备注: 添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static void lsys_set_steps(void *this, size_t steps)
{
    /* 将this指针转换为Lsys指针 */
    lsys_t* This = (lsys_t*)this;

    This->steps = steps;
}

/*-----------------------------------------------------------------------
// 名称: static void lsys_set_random(void *this, int random)
// 功能: 设置L-System是否为随机系统(同一字符多条替换规则，随机选择替换)
// 参数:
//       [void* this] ---------- 协议类对象指针
//       [int random] --- 是否为随机L-System(0---非随机，非0---随机)
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月13日
// 备注: 添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static void lsys_set_random(void *this, int random)
{
    /* 将this指针转换为Lsys指针 */
    lsys_t* This = (lsys_t*)this;

    This->is_random = random;

    /* 如果是随机系统则申请索引结构体数组 */
    /* 否则，则释放索引结构体数组 */
    if(This->is_random)
    {
        if(This->rule_count)
        {
            free(This->rule_count);
            This->rule_count = NULL;
        }
        if(This->rules_num != 0)
        {
            _realloc_rule_count(This, This->rules_num);
        }
    }
    else
    {
        if(This->rule_count)
        {
            free(This->rule_count);
            This->rule_count = NULL;
        }
    }
}

/*-----------------------------------------------------------------------
// 名称: static lsys_t * _realloc_rules(lsys_t *this, size_t size)
// 功能: 重新分配规则结构体数组占用的内存
// 参数:
//       [lsys_t* this] ---- 协议类对象指针
//       [size_t size] ----- 规则数量
// 返回: [lsys_t*] --- 自身对象的地址
// 作者: 耿楠
// 日期: 2022年11月11日
// 备注: 仅供内部函数调用，添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static lsys_t * _realloc_rules(lsys_t *this, size_t size)
{
    int i;

    this->rules_alloc = size;

    /* 重新分配空间 */
    this->rules = (rule_t *)realloc(this->rules, size * sizeof(rule_t));
    if(this->rules == NULL)
    {
        printf("Not enough memory for rules pointer array!\n");
        exit(1);
    }

    /* 初始化新增空间 */
    for(i = this->rules_num; i < this->rules_alloc; i++)
    {
        this->rules[i].ch = 'F';
        this->rules[i].rule = NULL;
    }

    return this;
}

/*-----------------------------------------------------------------------
// 名称: static lsys_t* _realloc_rule_count(lsys_t *this, size_t size)
// 功能: 重新分配规则索引结构体数组占用的内存
// 参数:
//       [lsys_t* this] ---- 协议类对象指针
//       [size_t size] ----- 规则索引数量(被替换字符的个数)
// 返回: [lsys_t*] --- 自身对象的地址
// 作者: 耿楠
// 日期: 2022年11月12日
// 备注: 仅供内部函数调用，添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static lsys_t* _realloc_rule_count(lsys_t *this, size_t size)
{
    this->rule_idx_total = size;

    /* 重新分配空间 */
    this->rule_count = (rule_count_t *)realloc(this->rule_count, size * sizeof(rule_count_t));
    if(this->rule_count == NULL)
    {
        printf("Not enough rule count array memory!\n");
        exit(1);
    }

    return this;
}

/*-----------------------------------------------------------------------
// 名称: static void _sort_rules(lsys_t* this)
// 功能: 对替换规则结构体数组进行排序
// 参数:
//       [lsys_t* this] ---- 协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月12日
// 备注: 仅供内部函数调用，添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static void _sort_rules(lsys_t* this)
{
    qsort(this->rules, this->rules_num, sizeof(rule_t), _qsort_cmp);
}

/*-----------------------------------------------------------------------
// 名称: int _qsort_cmp(const void* p, const void* q)
// 功能: 对替换规则结构体数组进行排序时需要的比较函数
// 参数:
//       [void* p] ---- 元素指针
//       [void* q] ---- 元素指针
// 返回: [int] --- 大于返回正数，小于返回负数，否则返回0
// 作者: 耿楠
// 日期: 2022年11月12日
// 备注: 仅供内部函数调用，添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
int _qsort_cmp(const void* p, const void* q)
{
    rule_t * pa = (rule_t*)p;
    rule_t * pb = (rule_t*)q;
    return (pa->ch - pb->ch);
}

/*-----------------------------------------------------------------------
// 名称: static size_t _create_rule_idx(lsys_t *this)
// 功能: 创建规则按被替换字符为key值的索引结构体数组
// 算法: 需要替换规则有序
// 参数:
//       [lsys_t* this] ---- 协议类对象指针
// 返回: [size_t] --- 索引结构体数组的大小
// 作者: 耿楠
// 日期: 2022年11月12日
// 备注: 仅供内部函数调用，添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static size_t _create_rule_idx(lsys_t *this)
{
    int i;
    char ch;
    int start = 0, total = 1;
    int idx = 0;

    /* 规则数不为0，记录第一个字符 */
    if(this->rules_num != 0)
    {
        ch = this->rules[0].ch;
        start = 0;
        this->rule_count[idx].ch = ch;
        this->rule_count[idx].start = start;
        this->rule_count[idx].total = total;
        idx++;
    }

    /* 遍历所有规则，统计每类规则的在有序数组中的起始位置和总数 */
    for(i = 1; i < this->rules_num; i ++)
    {
        if(this->rules[i].ch == ch)
        {
            total++;
            if(i == (this->rules_num - 1))
            {
                this->rule_count[idx - 1].total = total;
            }
        }
        else
        {
            this->rule_count[idx - 1].total = total;
            start = i;
            total = 1;
            ch = this->rules[i].ch;
            this->rule_count[idx].ch = ch;
            this->rule_count[idx].start = start;
            idx++;
        }
    }

    return idx;
}

/*-----------------------------------------------------------------------
// 名称: static int _lsys_cat(lsys_t* this, char ch, sds pre, sds next,
//                            sds (*pf_get_rule)(lsys_t *, char))
// 功能: 用传入的字符串重新拼接lsys字符串
// 参数:
//       [lsys_t* this] --- 协议类对象指针
//       [char ch] -------- 被替换字符
//       [sds pre] -------- 前导字符串
//       [sds next] ------- 后继字符串
         [sds (*pf_get_rule)(lsys_t *, char)] --- 提取规则字符串的函数指针
// 返回: [int] --- 返回替换字符串后一位的位置
// 作者: 耿楠
// 日期: 2022年11月14日
// 备注: 仅供内部函数调用，添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static int _lsys_cat(lsys_t* this, char ch, sds pre, sds next,
                     sds (*pf_get_rule)(lsys_t *, char))
{
    int k = 0;
    char buf[2] = {0};
    sds rep_str = NULL;

    /* 取得当前字符的替换规则(字符串) */
    rep_str = pf_get_rule(this, ch);

    /* 清空当前字符串 */
    sdsclear(this->lsys);

    /* 结果拼接 */
    this->lsys = sdscatsds(this->lsys, pre);
    if(rep_str != NULL)
    {
        this->lsys = sdscatsds(this->lsys, rep_str);
        /* 更新当前位置 */
        k = sdslen(rep_str);
    }
    else
    {
        buf[0] = ch;
        this->lsys = sdscat(this->lsys, buf);
        /* 更新当前位置 */
        k = 1;
    }
    this->lsys = sdscatsds(this->lsys, next);

    sdsfree(pre);
    sdsfree(next);

    return k;
}
/*-----------------------------------------------------------------------
// 名称: static sds _get_normal_rule(lsys_t *this, char ch)
// 功能: 返回指定字符的常规(非随机)替换规则(字符串)
// 参数:
//       [lsys_t* this] ---- 协议类对象指针
//       [char ch] --------- 被替换字符
// 返回: [sds] --- 替换规则(字符串)
// 作者: 耿楠
// 日期: 2022年11月13日
// 备注: 仅供内部函数调用，添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static sds _get_normal_rule(lsys_t *this, char ch)
{
    int i;
    /* int buf[1] = {0}; */
    sds rep_str = NULL;

    /* 遍历规则数组，找到匹配的替换字符，返回规则字符串 */
    for(i = 0; i < this->rules_num; i++)
    {
        if(this->rules[i].ch == ch)
        {
            rep_str = this->rules[i].rule;
            return rep_str;
        }
    }

    /* 未找到匹配的替换字符，返回NULL */
    /* buf[0] = ch; */
    /* rep_str = sdsnewlen(buf, 1); */

    return rep_str;
}

/*-----------------------------------------------------------------------
// 名称: static sds _get_random_rule(lsys_t *this, char ch)
// 功能: 随机返回指定字符的替换规则(字符串)
// 算法: 通过索引数组检索后，在排序后的规则数组中进行定位，返回替换规则(字符串)
// 参数:
//       [lsys_t* this] ---- 协议类对象指针
//       [char ch] --------- 被替换字符
// 返回: [sds] --- 替换规则(字符串)
// 作者: 耿楠
// 日期: 2022年11月13日
// 备注: 仅供内部函数调用，添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static sds _get_random_rule(lsys_t *this, char ch)
{
    int i, choice;
    /* int buf[1] = {0}; */
    sds rep_str = NULL;

    /* 遍历规则数组，找到匹配的替换字符，返回规则字符串 */
    for(i = 0; i < this->rule_idx_total; i++)
    {
        if(this->rule_count[i].ch == ch)
        {
            /* 根据当前字符的规则总数生成检索值 */
            choice = rand() % this->rule_count[i].total;

            /* 检索规则并返回 */
            rep_str = this->rules[this->rule_count[i].start + choice].rule;
            return rep_str;
        }
    }

    /* 未找到匹配的替换字符，返回NULL */
    /* buf[0] = ch; */
    /* rep_str = sdsnewlen(buf, 1); */

    return rep_str;
}

/*-----------------------------------------------------------------------
// 名称: static void _free_rules(lsys_t *this)
// 功能: 释放替换规则结构体数组占用的内存
// 参数:
//       [lsys_t* this] ---- 协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月11日
// 备注: 仅供内部函数调用，添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static void _free_rules(lsys_t *this)
{
    int i;

    if(this->rules != NULL)
    {
        for(i = 0; i < this->rules_alloc; i++)
        {
            sdsfree(this->rules[i].rule);
        }
        free(this->rules);
        this->rules = NULL;
    }
}

/*-----------------------------------------------------------------------
// 名称: static void lsys_print(void *this)
// 功能: 打印输出L-System对象中各个成员的值
// 参数:
//       [void* this] ---- 协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月13日
// 备注: 仅供内部函数调用，添加static是为了限制文件外部链接
//-----------------------------------------------------------------------*/
static void lsys_print(void *this)
{
    int i;

    /* 将this指针转换为Lsys指针 */
    lsys_t* This = (lsys_t*)this;

    printf("axiom = %s\n", This->axiom);

    printf("rules_nums = %ld\n", This->rules_num);
    printf("rules_alloc = %ld\n", This->rules_alloc);

    for(i = 0; i < This->rules_alloc; i++)
    {
        printf("%c ==> %s\n", This->rules[i].ch, This->rules[i].rule);
    }

    printf("steps = %ld\n", This->steps);

    printf("lsys = %s\n", This->lsys);

    if(This->is_random)
    {
        _sort_rules(This);
        This->rule_idx_total = _create_rule_idx(This);
        _realloc_rule_count(This, This->rule_idx_total);

        for(i = 0; i < This->rules_num; i++)
        {
            printf("%c==>%s\n", This->rules[i].ch, This->rules[i].rule);
        }

        printf("rule index total = %ld\n", This->rule_idx_total);

        for(i = 0; i < This->rule_idx_total; i++)
        {
            printf("rule_idx: %c(%d, %d)\n", This->rule_count[i].ch,
                    This->rule_count[i].start, This->rule_count[i].total);
        }
    }
}
