/*--------------------------------------------------------------------------------
 * Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
 * All rights reserved.
 *
 * 文件名称：drawlsys.h
 * 文件标识：见配置管理计划书
 * 摘要：l-system分形字符串解析绘制头文件。
 *
 * 当前版本：1.0.1
 * 作者：耿楠
 * 完成日期：2022年11月04日
 *
 * 取代版本：1.0
 * 原作者：耿楠
 * 完成日期：2020年12月03日
 ------------------------------------------------------------------------------------*/
#ifndef FRACTAL_H_INCLUDE
#define FRACTAL_H_INCLUDE

#include "turtle.h"

/* 解析L-System字符串，并用turtle绘图库绘制结果图形 */
void draw_lsys(turtle *, const char *, double, double, double, double, double);

#endif
