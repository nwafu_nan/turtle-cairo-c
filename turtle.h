/*--------------------------------------------------------------------------------
 * Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
 * All rights reserved.
 *
 * 文件名称：turtle.h
 * 文件标识：见配置管理计划书
 * 摘要：龟行绘图协议类封装头文件。
 *
 * 当前版本：1.0
 * 作者：耿楠
 * 完成日期：2022年11月04日
 *
 * 取代版本：无
 * 原作者：
 * 完成日期：
 ------------------------------------------------------------------------------------*/
#ifndef TURTLE_H_INCLUDE
#define TURTLE_H_INCLUDE

/* 乌龟结构体定义头文件 */
#include "structs.h"
#include "defs.h"

/* Cairo二维失量图形库 */
#include <cairo.h>

/* 龟行协议类(结构体)定义 */
typedef struct
{
    /* 乌龟状态变量 */
    turtle_t main_turtle;

    /* 画布宽和高 */
    int main_field_width;
    int main_field_height;

    /* Cairo库画布与绘图环境 */
    cairo_surface_t *surface;
    cairo_t *cr;

    /* Cairo结果文件名称 */
    const char *saved_filename;
    /* Cairo后端类型 */
    turtle_save_type_t saved_type;
    /* Cairo画布类型 */
    turtle_canvas_type_t canvas_type;

    /* 操作函数指针 */
    /* 系统初始化设置宽、高和文件名 */
    void (*init)(void *, int, int, const char*, turtle_save_type_t, turtle_canvas_type_t);

    /* 系统复位，包括位置、方向、颜色和画笔状态：
       位置：(0, 0)
       方向: (0度)，向右
       画笔: 黑色，按下
    */
    void (*reset)(void *);

    /* 乌龟状态压栈 */
    void (*push)(void *);

    /* 龟状态出栈 */
    void (*pop)(void *);

    /* 将当前绘制内容保存为指定的文件 */
    void (*save)(void *);

    /* 设置当前画笔宽度 */
    void (*setlinewidth)(void *, double);
    void (*pensize)(void *, double);
    void (*penwidth)(void *, double);
    /* 获取当前画笔宽度 */
    double (*getlinewidth)(void *);
    double (*getpensize)(void *);
    double (*getpenwidth)(void *);

    /* 设置当前画笔颜色，颜色使用RGB表示，(0, 0, 0)表示黑色，(255, 255, 255)表示白色 */
    void (*setpencolor)(void *, unsigned int);
    void (*penrgb)(void *, unsigned char, unsigned char, unsigned char);
    void (*penrgba)(void *, unsigned char, unsigned char, unsigned char, unsigned char);
    void (*penbit32)(void *, unsigned int);
    void (*penname)(void *, char *);
    void (*penenum)(void *, color_name_t);

    /* 设置当前填充颜色，颜色使用RGB表示，(0, 0, 0)表示黑色，(255, 255, 255)表示白色 */
    void (*setfillcolor)(void *, unsigned int);
    void (*fillrgb)(void *, unsigned char, unsigned char, unsigned char);
    void (*fillrgba)(void *, unsigned char, unsigned char, unsigned char, unsigned char);
    void (*fillbit32)(void *, unsigned int);
    void (*fillname)(void *, char *);
    void (*fillenum)(void *, color_name_t);

    /* 设置画笔为提笔状态 */
    void (*penup)(void *);
    void (*up)(void *);
    void (*pu)(void *);

    /* 设置画笔为落笔状态 */
    void (*pendown)(void *);
    void (*down)(void *);
    void (*pd)(void *);

    /* 向前爬行指定距离，如果画笔为落笔状态，则绘制一条直线 */
    void (*forward)(void *, double);
    void (*fd)(void *, double);

    /* 向后爬行指定距离，如果画笔为落笔下状态，则绘制一条直线 */
    void (*backward)(void *, double);
    void (*back)(void *, double);
    void (*bk)(void *, double);

    /* 向左转指定角度 */
    void (*left)(void *, double);
    void (*lt)(void *, double);

    /* 向右转指定角度 */
    void (*right)(void *, double);
    void (*rt)(void *, double);

    /* 将乌龟按当前方向移动指定距离，如果画笔为按下状态，则画一条直线 */
    void (*go)(void *, double);
    /* 将乌龟移动到指定位置，如果画笔为按下状态，则画一条直线 */
    void (*to)(void *, double , double);

    /* 绘制圆弧 */
    void (*arc)(void *, double, double, int);
    void (*circle)(void *, double, double, int);

    /* 在当前位置绘制一个大小为1个像素的点(忽略笔画状态) */
    void (*dot)(void *);

    /* 向左扫射-->左转90度向前爬行指定距离后右转90度 */
    void (*strafeleft)(void *, double);
    void (*sl)(void *, double);
    /* 向右扫射-->右转90度向前爬行指定距离后左转90度 */
    void (*straferight)(void *, double);
    void (*sr)(void *, double);

    /* 在当前位置绘制一个字符串 */
    void (*putstr)(void *, char *, double, char *);

    /* 开始填充，需在绘制多边形之前调用此函数以激活填充算法. */
    void (*beginfill)(void *);
    void (*bf)(void *);
    /* 结束填充，需在绘制多边形后调用此函数以结束填充算法。*/
    void (*endfill)(void *);
    void (*ef)(void *);

    /* 设置乌龟朝向，0为向右，90为向上 */
    void (*setheading)(void *, double);
    void (*seth)(void *, double);

    /* 设置乌龟回到原位 */
    void (*sethome)(void *);
    void (*home)(void *);

    /* 设置乌龟x位置 */
    void (*setx)(void *, double);
    void (*sx)(void *, double);
    /* 设置乌龟y位置 */
    void (*sety)(void *, double);
    void (*sy)(void *, double);
    /* 返回当前乌龟的X坐标 */
    double (*getx)(void *);
    double (*gx)(void *);
    /* 返回当前乌龟的Y坐标 */
    double (*gety)(void *);
    double (*gy)(void *);

    /* 绘制一个乌龟标志图形 */
    void (*logo)(void *);
} turtle;

/* 创建龟行系统对象 */
turtle * turtle_ctor(turtle *);
/* 销毁龟行系统对象 */
void turtle_dtor(turtle *);

#endif

