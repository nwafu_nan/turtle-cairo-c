/* 代码选自《C语言程序设计:现代方法(第二版)》P349 */
/* 在此仅对其添加了必要注释 */
/* 当前版本：1.0
 * 作者：耿楠
 * 完成日期：2020年12月03日 */
#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

/* 用链表实现栈数据结构 */
struct node
{
    turtle_t data;
    struct node *next;
};

/* 栈顶指针 */
struct node *top = NULL;

/* 错误响应 */
static void terminate(const char *message)
{
    printf("%s\n", message);
    exit(EXIT_FAILURE);
}

/* 清空栈 */
void make_empty(void)
{
    while(!is_empty())
    {
        pop();
    }
}

/* 判断栈是否为空 */
bool is_empty(void)
{
    return top == NULL;
}

/* 判断栈是否为满(用链表实现，永远不满) */
bool is_full(void)
{
    return false;
}

/* 将一个turtle_t对象压栈 */
void push(turtle_t main_turtle)
{
    /* 创建结点 */
    struct node *new_node = malloc(sizeof(struct node));
    if(new_node == NULL)
    {
        terminate("Error in push: stack is full.");
    }

    /* 链入链表 */
    new_node->data = main_turtle;
    new_node->next = top;
    /* 调整栈顶指针 */
    top = new_node;
}

/* 弹出栈顶元素 */
turtle_t pop(void)
{
    struct node *old_top;
    turtle_t t;

    /* 栈为空，构造“乌龟”初始状态后返回 */
    if (is_empty())
    {
        /* 复位至图像中心 */
        t.xpos = 0.0;
        t.ypos = 0.0;

        /* 方向向右(0度) */
        t.heading = 0.0;

        /* 默认颜色为黑色 */
        t.pen_color.red = 0;
        t.pen_color.green = 0;
        t.pen_color.blue = 0;

        /* 默认填充色为黑色 */
        t.fill_color.red = 0;
        t.fill_color.green = 255;
        t.fill_color.blue = 0;

        /* 按下画笔 */
        t.pendown = true;

        /* 默认填充状态为关闭 */
        t.filled = false;
    }

    /* 记录栈顶指针 */
    old_top = top;
    /* 取得栈顶元素 */
    t = top->data;
    /* 调整栈顶指针 */
    top = top->next;
    /* 释放栈顶元素 */
    free(old_top);

    /* 返回 */
    return t;
}
