/* 代码选自《C语言程序设计:现代方法(第二版)》P349 */
/* 在此仅对其添加了必要注释 */
/* 当前版本：1.0
 * 作者：耿楠
 * 完成日期：2020年12月03日 */
#ifndef STACK_H_INCLUDE
#define STACK_H_INCLUDE

#include "structs.h"
#include <stdbool.h> /* C99 only */

void make_empty(void);
bool is_empty(void);
bool is_full(void);
void push(turtle_t);
turtle_t pop(void);

#endif
