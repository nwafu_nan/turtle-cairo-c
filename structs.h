/*--------------------------------------------------------------------------------
 * Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
 * All rights reserved.
 *
 * 文件名称：structs.h
 * 文件标识：见配置管理计划书
 * 摘要：龟行绘图协议类结构体定义。
 *
 * 当前版本：1.0
 * 作者：耿楠
 * 完成日期：2020年12月03日
 *
 * 取代版本：
 * 原作者：
 * 完成日期：
 ------------------------------------------------------------------------------------*/
#ifndef STRUCTS_H_INCLUDE
#define STRUCTS_H_INCLUDE

#include <stdbool.h> /* C99 only */

/* 颜色结构体 */
typedef struct
{
    unsigned char red;
    unsigned char green;
    unsigned char blue;
    unsigned char alpha;
} rgba_t;


/* 龟形状态结构体 */
typedef struct
{
    double  xpos;       // x坐标
    double  ypos;       // y坐标
    double  heading;    // 爬行方向

    double  line_width;  // 线宽
    rgba_t   pen_color;   // 画笔颜色
    rgba_t   fill_color;  // 填充颜色
    bool    pendown;     // 绘制状态
    bool    filled;      // 填充状态
} turtle_t;

#endif
