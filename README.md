# C语言海龟绘图(turtle-cairo-c)
----------

## 介绍
----------
一个基于[cairo二维图形库](https://www.cairographics.org)用C语言实现的简单海龟绘图系统。

#### 绘制样例
----------
<img src="./screenshots/screenshot01.png" height="20%"><img src="./screenshots/screenshot02.png" height="20%"><img src="./screenshots/screenshot03.png" height="20%">

## 说明
----------
1. 用C语言的结构体将海龟绘图中的乌龟状态、cairo库绘图状态等属性和对乌龟的控制及绘图操作封装为协议类，从而实现了对属性和操作的统一管理。
2. 用栈结构实现了乌龟状态保存与恢复，从而实现了分支图形的生成。
3. 封装了cairo二维图形库的PNG、SVG或PDF绘图后端，可以根据初始化参数按需选择生成的图像格式。
4. 目前仅在Linux下用GCC进行了测试，不保证其它平台能够正常编译。如果未安装cairo库，在Ubuntu系统中可使用`sudo apt install libcairo2-dev`安装cairo库。
5. 所有代码用纯C语言实现。
6. 在终端窗口中用make命令实现构建。

## 接口
----------
所有接口在`turtle.h`中定义，在接口中，所有操作通过函数指针实现了封装，并实现了同一操作的别名设计。具体为：
```c
#ifndef TURTLE_H_INCLUDE
#define TURTLE_H_INCLUDE
...
/* 龟行协议类(结构体)定义 */
typedef struct
{
    ...
    /* 各个函数指针指向的函数的第1个参数是一个void *this 指针，用于指向当前调用对象 */
    /* 对部分操作，通过将多个函数指针指向同一函数实现其别名设计 */

    /* 系统初始化设置宽、高和文件名 */
    void (*init)(void *this,
                 int with,                      /* 图像宽度(像素单位) */
                 int height,                    /* 图像高度(像素单位) */
                 const char*filename,           /* 存储图像的文件名称 */
                 turtle_save_type_t saved_type, /* 图像类型(png/svg/pdf) */
                 turtle_canvas_type_t           /* 画布类型(像素坐标/归一化坐标) */
                 );

    /* 系统复位，包括位置(0.0, 0.0)、方向(向右)、颜色(黑色)和画笔(落笔)状态*/
    void (*reset)(void *this);
    /* 乌龟状态压栈 */
    void (*push)(void *this);
    /* 龟状态出栈 */
    void (*pop)(void *this);
    /* 将当前绘制内容保存为指定的文件 */
    void (*save)(void *this);

    /* 设置当前画笔宽度 */
    void (*setlinewidth)(void *this, double line_width);
    void (*pensize)(void *this, double line_width);
    void (*penwidth)(void *this, double line_width);

    /* 设置当前画笔颜色 */
    /* 32位16进制颜色值 */
    void (*setpencolor)(void *this, unsigned int color);

    /* RGB分量值 */
    void (*penrgb)(void *this,
                   unsigned char r, unsigned char g, unsigned char b);

    /* RGBA分量值 */
    void (*penrgba)(void *this,
                    unsigned char r, unsigned char g, unsigned char b,
                    unsigned char a);

    /* 32位16进制颜色值(setpencolor的别名) */
    void (*penbit32)(void *this, unsigned int color);

    /* 颜色字符串名称 */
    void (*penname)(void *this, char *color_name);

    /* 颜色枚举常量 */
    void (*penenum)(void *, color_name_t color_idx);

    /* 设置当前填充颜色 */
    /* 32位16进制颜色值 */
    void (*setfillcolor)(void *this, unsigned int color);

    /* RGB分量值 */
    void (*fillrgb)(void *this,
                    unsigned char r, unsigned char g, unsigned char b);

    /* RGBA分量值 */
    void (*fillrgba)(void *this,
                     unsigned char r, unsigned char g, unsigned char b,
                     unsigned char a);

    /* 32位16进制颜色值(setfillcolor的别名) */
    void (*fillbit32)(void *this, unsigned int color);

    /* 颜色字符串名称 */
    void (*fillname)(void *this , char *color_name);

    /* 颜色枚举常量 */
    void (*fillenum)(void *this, color_name_t color_idx);

    /* 设置画笔为提笔状态 */
    void (*penup)(void *this);
    void (*up)(void *this);
    void (*pu)(void *this);

    /* 设置画笔为落笔状态 */
    void (*pendown)(void *this);
    void (*down)(void *this);
    void (*pd)(void *this);

    /* 向前爬行指定距离，如果画笔为落笔状态，则绘制一条直线 */
    void (*forward)(void *this, double dist);
    void (*fd)(void *this, double dist);

    /* 向后爬行指定距离，如果画笔为落笔下状态，则绘制一条直线 */
    void (*backward)(void *this, double dist);
    void (*back)(void *this, double dist);
    void (*bk)(void *this, double dist);

    /* 向左转指定角度 */
    void (*left)(void *this, double angle);
    void (*lt)(void *this, double angle);

    /* 向右转指定角度 */
    void (*right)(void *this, double angle);
    void (*rt)(void *this, double angle);

    /* 将乌龟按当前方向移动指定距离，如果画笔为按下状态，则画一条直线 */
    void (*go)(void *this, double dist);

    /* 将乌龟移动到指定位置，如果画笔为按下状态，则画一条直线 */
    void (*to)(void *this, double x, double y);

    /* 绘制圆弧(直线段逼近绘制) */
    void (*arc)(void *this,
                double radius, /* 半径，圆心在垂直于当前方向的直线上，正值在当前位置左侧, 负值在右侧 */
                double angle,  /* 扇角，正值向前画，负值向后画 */
                int steps      /* 逼近直线段分段数 */
                );
    void (*circle)(void *this, double radius, double angle, int stes);

    /* 在当前位置绘制一个大小为1个像素的点(忽略笔画状态) */
    void (*dot)(void *this);

    /* 向左扫射-->左转90度向前爬行指定距离后右转90度 */
    void (*strafeleft)(void *this, double dist);
    void (*sl)(void *this, double dist);
    /* 向右扫射-->右转90度向前爬行指定距离后左转90度 */
    void (*straferight)(void *this, double dist);
    void (*sr)(void *this, double dist);

    /* 在当前位置绘制一个字符串 */
    void (*putstr)(void *this,
                   char *, /* 需要绘制的字符串 */
                   double, /* 字号 */
                   char*   /* 字体名称(可以用fc-list命令查询) */
                   );

    /* 开始填充，需在绘制多边形之前调用此函数以激活填充算法. */
    void (*beginfill)(void *this);
    void (*bf)(void *this);
    /* 结束填充，需在绘制多边形后调用此函数以结束填充算法。*/
    void (*endfill)(void *this);
    void (*ef)(void *this);

    /* 设置乌龟朝向，0为向右，90为向上 */
    void (*setheading)(void *this, double angle);
    void (*seth)(void *this, double angle);

    /* 设置乌龟回到原位 */
    void (*sethome)(void *this);
    void (*home)(void *this);

    /* 设置乌龟x位置 */
    void (*setx)(void *this, double x);
    void (*sx)(void *this, double x);
    /* 设置乌龟y位置 */
    void (*sety)(void *this, double y);
    void (*sy)(void *this, double y);

    /* 返回当前乌龟的X坐标 */
    double (*getx)(void *this);
    double (*gx)(void *this);
    /* 返回当前乌龟的Y坐标 */
    double (*gety)(void *this);
    double (*gy)(void *this);

    /* 绘制一个乌龟标志图形 */
    void (*logo)(void *this);
} turtle;

turtle * turtle_ctor(turtle *);
void turtle_dtor(turtle *);

#endif
```
其详情请参阅源代码文件。

## 绘制方式
----------
1. 用`#include "turtle.h"**包含**turtle.h头文件。
2. 在需要绘图的函数中声明一个如`t1`的`turtle`结构体类型的对象(变量)。
3. 调用new函数初始化对象，如：`turtle_ctor(&t1);`，然后通过对象访问其**函数指针成员**实现对具体操作函数的调用。
4. 对过对象调用init成员初始化海龟系统，如：`t1.init(&t1, ...);`
5. 通过对象调用各类操作函数，按海龟绘图的模式进行绘图，如：`t1.forward(&t1, 50);`。
6. 结束绘制后，通过对象调用save成员将绘制的图像存入指定文件，如：`t1.save(&t1);`。
7. 调用del操作释放资源，如：`turtle_dtor(&t1);`。

**注意**：在调用操作函数时，**务必要为各函数的第1个实参传入当前turtle对象的地址**！

使用该turtle海龟绘图系统实现绘图的一个完整示例为：
```c
#include "turtle.h"

int main (int argc, char *argv[])
{
    turtle t1;

    turtle_ctor(&t1);
    t1.init(&t1, 300, 300, "01.pdf", TURTLE_OUT_TYPE_PDF, TURTLE_CANVAS_PIXEL);

    /* 起始位置定位 */
    t1.up(&t1);
    t1.to(&t1, -50, -50);
    t1.down(&t1);
    /* 直接用turtle函数绘制 */
    t1.forward(&t1, 100);
    t1.left(&t1, 90);
    t1.forward(&t1, 100);
    t1.left(&t1, 90);
    t1.forward(&t1, 100);
    t1.left(&t1, 90);
    t1.forward(&t1, 100);
    t1.save(&t1);
    turtle_dtor(&t1);

    return 0;
}
```

其它样例，请参阅源代码及其注释。

## 颜色常量

#### 颜色字符串名称
----------
```c
/* 颜色名称与32位十六进制值对应结构体常量 */
const bit32_color_t color_sets[] =
{
    {"alice_blue",              0xFFF0F8FF},
    {"antique_white",           0xFFFAEBD7},
    {"aqua",                    0xFF00FFFF},
    {"aquamarine",              0xFF7FFFD4},
    {"azure",                   0xFFF0FFFF},
    {"beige",                   0xFFF5F5DC},
    {"bisque",                  0xFFFFE4C4},
    {"black",                   0xFF000000},
    {"blanched_almond",         0xFFFFEBCD},
    {"blue",                    0xFF0000FF},
    {"blue_violet",             0xFF8A2BE2},
    {"brown",                   0xFFA52A2A},
    {"burly_wood",              0xFFDEB887},
    {"cadet_blue",              0xFF5F9EA0},
    {"chartreuse",              0xFF7FFF00},
    {"chocolate",               0xFFD2691E},
    {"coral",                   0xFFFF7F50},
    {"cornflower_blue",         0xFF6495ED},
    {"cornsilk",                0xFFFFF8DC},
    {"crimson",                 0xFFDC143C},
    {"cyan",                    0xFF00FFFF},
    {"dark_blue",               0xFF00008B},
    {"dark_cyan",               0xFF008B8B},
    {"dark_golden_rod",         0xFFB8860B},
    {"dark_gray",               0xFFA9A9A9},
    {"dark_green",              0xFF006400},
    {"dark_khaki",              0xFFBDB76B},
    {"dark_magenta",            0xFF8B008B},
    {"dark_olive_green",        0xFF556B2F},
    {"dark_orange",             0xFFFF8C00},
    {"dark_orchid",             0xFF9932CC},
    {"dark_red",                0xFF8B0000},
    {"dark_salmon",             0xFFE9967A},
    {"dark_sea_green",          0xFF8FBC8F},
    {"dark_slate_blue",         0xFF483D8B},
    {"dark_slate_gray",         0xFF2F4F4F},
    {"dark_turquoise",          0xFF00CED1},
    {"dark_violet",             0xFF9400D3},
    {"deep_pink",               0xFFFF1493},
    {"deep_sky_blue",           0xFF00BFFF},
    {"dim_gray",                0xFF696969},
    {"dodger_blue",             0xFF1E90FF},
    {"fire_brick",              0xFFB22222},
    {"floral_white",            0xFFFFFAF0},
    {"forest_green",            0xFF228B22},
    {"fuchsia",                 0xFFFF00FF},
    {"gainsboro",               0xFFDCDCDC},
    {"ghost_white",             0xFFF8F8FF},
    {"gold",                    0xFFFFD700},
    {"golden_rod",              0xFFDAA520},
    {"gray",                    0xFF808080},
    {"green",                   0xFF008000},
    {"green_yellow",            0xFFADFF2F},
    {"honey_dew",               0xFFF0FFF0},
    {"hot_pink",                0xFFFF69B4},
    {"indian_red",              0xFFCD5C5C},
    {"indigo",                  0xFF4B0082},
    {"ivory",                   0xFFFFFFF0},
    {"khaki",                   0xFFF0E68C},
    {"lavender",                0xFFE6E6FA},
    {"lavender_blush",          0xFFFFF0F5},
    {"lawn_green",              0xFF7CFC00},
    {"lemon_chiffon",           0xFFFFFACD},
    {"light_blue",              0xFFADD8E6},
    {"light_coral",             0xFFF08080},
    {"light_cyan",              0xFFE0FFFF},
    {"light_golden_rod_yellow", 0xFFFAFAD2},
    {"light_gray",              0xFFD3D3D3},
    {"light_green",             0xFF90EE90},
    {"light_pink",              0xFFFFB6C1},
    {"light_salmon",            0xFFFFA07A},
    {"light_sea_green",         0xFF20B2AA},
    {"light_sky_blue",          0xFF87CEFA},
    {"light_slate_gray",        0xFF778899},
    {"light_steel_blue",        0xFFB0C4DE},
    {"light_yellow",            0xFFFFFFE0},
    {"lime",                    0xFF00FF00},
    {"lime_green",              0xFF32CD32},
    {"linen",                   0xFFFAF0E6},
    {"magenta",                 0xFFFF00FF},
    {"maroon",                  0xFF800000},
    {"medium_aqua_marine",      0xFF66CDAA},
    {"medium_blue",             0xFF0000CD},
    {"medium_orchid",           0xFFBA55D3},
    {"medium_purple",           0xFF9370DB},
    {"medium_sea_green",        0xFF3CB371},
    {"medium_slate_blue",       0xFF7B68EE},
    {"medium_spring_green",     0xFF00FA9A},
    {"medium_turquoise",        0xFF48D1CC},
    {"medium_violet_red",       0xFFC71585},
    {"midnight_blue",           0xFF191970},
    {"mint_cream",              0xFFF5FFFA},
    {"misty_rose",              0xFFFFE4E1},
    {"moccasin",                0xFFFFE4B5},
    {"navajo_white",            0xFFFFDEAD},
    {"navy",                    0xFF000080},
    {"old_lace",                0xFFFDF5E6},
    {"olive",                   0xFF808000},
    {"olive_drab",              0xFF6B8E23},
    {"orange",                  0xFFFFA500},
    {"orange_red",              0xFFFF4500},
    {"orchid",                  0xFFDA70D6},
    {"pale_golden_rod",         0xFFEEE8AA},
    {"pale_green",              0xFF98FB98},
    {"pale_turquoise",          0xFFAFEEEE},
    {"pale_violet_red",         0xFFDB7093},
    {"papaya_whip",             0xFFFFEFD5},
    {"peach_puff",              0xFFFFDAB9},
    {"peru",                    0xFFCD853F},
    {"pink",                    0xFFFFC0CB},
    {"plum",                    0xFFDDA0DD},
    {"powder_blue",             0xFFB0E0E6},
    {"purple",                  0xFF800080},
    {"rebecca_purple",          0xFF663399},
    {"red",                     0xFFFF0000},
    {"rosy_brown",              0xFFBC8F8F},
    {"royal_blue",              0xFF4169E1},
    {"saddle_brown",            0xFF8B4513},
    {"salmon",                  0xFFFA8072},
    {"sandy_brown",             0xFFF4A460},
    {"sea_green",               0xFF2E8B57},
    {"sea_shell",               0xFFFFF5EE},
    {"sienna",                  0xFFA0522D},
    {"silver",                  0xFFC0C0C0},
    {"sky_blue",                0xFF87CEEB},
    {"slate_blue",              0xFF6A5ACD},
    {"slate_gray",              0xFF708090},
    {"snow",                    0xFFFFFAFA},
    {"spring_green",            0xFF00FF7F},
    {"steel_blue",              0xFF4682B4},
    {"tan",                     0xFFD2B48C},
    {"teal",                    0xFF008080},
    {"thistle",                 0xFFD8BFD8},
    {"tomato",                  0xFFFF6347},
    {"turquoise",               0xFF40E0D0},
    {"violet",                  0xFFEE82EE},
    {"wheat",                   0xFFF5DEB3},
    {"white",                   0xFFFFFFFF},
    {"white_smoke",             0xFFF5F5F5},
    {"yellow",                  0xFFFFFF00},
    {"yellow_green",            0xFF9ACD32}
};
```
#### 颜色枚举常量
----------
```c
/* 颜色名称枚举常量 */
typedef enum _color_sets_type
{
    TURTLE_COLOR_ALICE_BLUE,
    TURTLE_COLOR_ANTIQUE_WHITE,
    TURTLE_COLOR_AQUA,
    TURTLE_COLOR_AQUAMARINE,
    TURTLE_COLOR_AZURE,
    TURTLE_COLOR_BEIGE,
    TURTLE_COLOR_BISQUE,
    TURTLE_COLOR_BLACK,
    TURTLE_COLOR_BLANCHED_ALMOND,
    TURTLE_COLOR_BLUE,
    TURTLE_COLOR_BLUE_VIOLET,
    TURTLE_COLOR_BROWN,
    TURTLE_COLOR_BURLY_WOOD,
    TURTLE_COLOR_CADET_BLUE,
    TURTLE_COLOR_CHARTREUSE,
    TURTLE_COLOR_CHOCOLATE,
    TURTLE_COLOR_CORAL,
    TURTLE_COLOR_CORNFLOWER_BLUE,
    TURTLE_COLOR_CORNSILK,
    TURTLE_COLOR_CRIMSON,
    TURTLE_COLOR_CYAN,
    TURTLE_COLOR_DARK_BLUE,
    TURTLE_COLOR_DARK_CYAN,
    TURTLE_COLOR_DARK_GOLDEN_ROD,
    TURTLE_COLOR_DARK_GRAY,
    TURTLE_COLOR_DARK_GREEN,
    TURTLE_COLOR_DARK_KHAKI,
    TURTLE_COLOR_DARK_MAGENTA,
    TURTLE_COLOR_DARK_OLIVE_GREEN,
    TURTLE_COLOR_DARK_ORANGE,
    TURTLE_COLOR_DARK_ORCHID,
    TURTLE_COLOR_DARK_RED,
    TURTLE_COLOR_DARK_SALMON,
    TURTLE_COLOR_DARK_SEA_GREEN,
    TURTLE_COLOR_DARK_SLATE_BLUE,
    TURTLE_COLOR_DARK_SLATE_GRAY,
    TURTLE_COLOR_DARK_TURQUOISE,
    TURTLE_COLOR_DARK_VIOLET,
    TURTLE_COLOR_DEEP_PINK,
    TURTLE_COLOR_DEEP_SKY_BLUE,
    TURTLE_COLOR_DIM_GRAY,
    TURTLE_COLOR_DODGER_BLUE,
    TURTLE_COLOR_FIRE_BRICK,
    TURTLE_COLOR_FLORAL_WHITE,
    TURTLE_COLOR_FOREST_GREEN,
    TURTLE_COLOR_FUCHSIA,
    TURTLE_COLOR_GAINSBORO,
    TURTLE_COLOR_GHOST_WHITE,
    TURTLE_COLOR_GOLD,
    TURTLE_COLOR_GOLDEN_ROD,
    TURTLE_COLOR_GRAY,
    TURTLE_COLOR_GREEN,
    TURTLE_COLOR_GREEN_YELLOW,
    TURTLE_COLOR_HONEY_DEW,
    TURTLE_COLOR_HOT_PINK,
    TURTLE_COLOR_INDIAN_RED,
    TURTLE_COLOR_INDIGO,
    TURTLE_COLOR_IVORY,
    TURTLE_COLOR_KHAKI,
    TURTLE_COLOR_LAVENDER,
    TURTLE_COLOR_LAVENDER_BLUSH,
    TURTLE_COLOR_LAWN_GREEN,
    TURTLE_COLOR_LEMON_CHIFFON,
    TURTLE_COLOR_LIGHT_BLUE,
    TURTLE_COLOR_LIGHT_CORAL,
    TURTLE_COLOR_LIGHT_CYAN,
    TURTLE_COLOR_LIGHT_GOLDEN_ROD_YELLOW,
    TURTLE_COLOR_LIGHT_GRAY,
    TURTLE_COLOR_LIGHT_GREEN,
    TURTLE_COLOR_LIGHT_PINK,
    TURTLE_COLOR_LIGHT_SALMON,
    TURTLE_COLOR_LIGHT_SEA_GREEN,
    TURTLE_COLOR_LIGHT_SKY_BLUE,
    TURTLE_COLOR_LIGHT_SLATE_GRAY,
    TURTLE_COLOR_LIGHT_STEEL_BLUE,
    TURTLE_COLOR_LIGHT_YELLOW,
    TURTLE_COLOR_LIME,
    TURTLE_COLOR_LIME_GREEN,
    TURTLE_COLOR_LINEN,
    TURTLE_COLOR_MAGENTA,
    TURTLE_COLOR_MAROON,
    TURTLE_COLOR_MEDIUM_AQUA_MARINE,
    TURTLE_COLOR_MEDIUM_BLUE,
    TURTLE_COLOR_MEDIUM_ORCHID,
    TURTLE_COLOR_MEDIUM_PURPLE,
    TURTLE_COLOR_MEDIUM_SEA_GREEN,
    TURTLE_COLOR_MEDIUM_SLATE_BLUE,
    TURTLE_COLOR_MEDIUM_SPRING_GREEN,
    TURTLE_COLOR_MEDIUM_TURQUOISE,
    TURTLE_COLOR_MEDIUM_VIOLET_RED,
    TURTLE_COLOR_MIDNIGHT_BLUE,
    TURTLE_COLOR_MINT_CREAM,
    TURTLE_COLOR_MISTY_ROSE,
    TURTLE_COLOR_MOCCASIN,
    TURTLE_COLOR_NAVAJO_WHITE,
    TURTLE_COLOR_NAVY,
    TURTLE_COLOR_OLD_LACE,
    TURTLE_COLOR_OLIVE,
    TURTLE_COLOR_OLIVE_DRAB,
    TURTLE_COLOR_ORANGE,
    TURTLE_COLOR_ORANGE_RED,
    TURTLE_COLOR_ORCHID,
    TURTLE_COLOR_PALE_GOLDEN_ROD,
    TURTLE_COLOR_PALE_GREEN,
    TURTLE_COLOR_PALE_TURQUOISE,
    TURTLE_COLOR_PALE_VIOLET_RED,
    TURTLE_COLOR_PAPAYA_WHIP,
    TURTLE_COLOR_PEACH_PUFF,
    TURTLE_COLOR_PERU,
    TURTLE_COLOR_PINK,
    TURTLE_COLOR_PLUM,
    TURTLE_COLOR_POWDER_BLUE,
    TURTLE_COLOR_PURPLE,
    TURTLE_COLOR_REBECCA_PURPLE,
    TURTLE_COLOR_RED,
    TURTLE_COLOR_ROSY_BROWN,
    TURTLE_COLOR_ROYAL_BLUE,
    TURTLE_COLOR_SADDLE_BROWN,
    TURTLE_COLOR_SALMON,
    TURTLE_COLOR_SANDY_BROWN,
    TURTLE_COLOR_SEA_GREEN,
    TURTLE_COLOR_SEA_SHELL,
    TURTLE_COLOR_SIENNA,
    TURTLE_COLOR_SILVER,
    TURTLE_COLOR_SKY_BLUE,
    TURTLE_COLOR_SLATE_BLUE,
    TURTLE_COLOR_SLATE_GRAY,
    TURTLE_COLOR_SNOW,
    TURTLE_COLOR_SPRING_GREEN,
    TURTLE_COLOR_STEEL_BLUE,
    TURTLE_COLOR_TAN,
    TURTLE_COLOR_TEAL,
    TURTLE_COLOR_THISTLE,
    TURTLE_COLOR_TOMATO,
    TURTLE_COLOR_TURQUOISE,
    TURTLE_COLOR_VIOLET,
    TURTLE_COLOR_WHEAT,
    TURTLE_COLOR_WHITE,
    TURTLE_COLOR_WHITE_SMOKE,
    TURTLE_COLOR_YELLOW,
    TURTLE_COLOR_YELLOW_GREEN,
    TURTLE_COLOR_NUMS
}color_name_t;
```

## 贡献
----------
由于受能力所限，无保证代码设计的准确性和合理性。如果您有任何改进意见或者功能需求，欢迎提交 [issue](https://gitee.com/nwafu_nan/turtle-cairo-c/issues) 或 [pull request](https://gitee.com/nwafu_nan/turtle-cairo-c/pulls)。
