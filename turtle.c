/*--------------------------------------------------------------------------------
 * Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
 * All rights reserved.
 *
 * 文件名称：turtle.c
 * 文件标识：见配置管理计划书
 * 摘要：龟行绘图协议类封装实现文件。
 *
 * 当前版本：1.0
 * 作者：耿楠
 * 完成日期：2022年11月04日
 *
 * 取代版本：无
 * 原作者：
 * 完成日期：
 *
 * 备注：
 * 龟行绘图原始代码参考https://w3.cs.jmu.edu/lam2mo/cs240_2015_08/turtle.html
 * 主要变化有：
 * 1. 为保持使用习惯，参考Python的turtle逻辑重新对代码重新进行了设计
 * 2. 用栈数据结构实现了乌龟状态的无限制保存与恢复
 * 3. 用函数指针将数据与操作进行封装，实现了龟行绘图协议类的设计
 * 4. 基于Cairo二维图形库实现了PNG、SVG或PDF绘制后端，从而取代原BMP绘制方案
 ------------------------------------------------------------------------------------*/
#include "turtle.h"
#include "defs.h"
#include "stack.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <cairo-pdf.h>
#include <cairo-svg.h>

/* 颜色名称与十六进制值对应结构体常量 */
const bit32_color_t color_sets[] =
{
    {"alice_blue",              0xFFF0F8FF},
    {"antique_white",           0xFFFAEBD7},
    {"aqua",                    0xFF00FFFF},
    {"aquamarine",              0xFF7FFFD4},
    {"azure",                   0xFFF0FFFF},
    {"beige",                   0xFFF5F5DC},
    {"bisque",                  0xFFFFE4C4},
    {"black",                   0xFF000000},
    {"blanched_almond",         0xFFFFEBCD},
    {"blue",                    0xFF0000FF},
    {"blue_violet",             0xFF8A2BE2},
    {"brown",                   0xFFA52A2A},
    {"burly_wood",              0xFFDEB887},
    {"cadet_blue",              0xFF5F9EA0},
    {"chartreuse",              0xFF7FFF00},
    {"chocolate",               0xFFD2691E},
    {"coral",                   0xFFFF7F50},
    {"cornflower_blue",         0xFF6495ED},
    {"cornsilk",                0xFFFFF8DC},
    {"crimson",                 0xFFDC143C},
    {"cyan",                    0xFF00FFFF},
    {"dark_blue",               0xFF00008B},
    {"dark_cyan",               0xFF008B8B},
    {"dark_golden_rod",         0xFFB8860B},
    {"dark_gray",               0xFFA9A9A9},
    {"dark_green",              0xFF006400},
    {"dark_khaki",              0xFFBDB76B},
    {"dark_magenta",            0xFF8B008B},
    {"dark_olive_green",        0xFF556B2F},
    {"dark_orange",             0xFFFF8C00},
    {"dark_orchid",             0xFF9932CC},
    {"dark_red",                0xFF8B0000},
    {"dark_salmon",             0xFFE9967A},
    {"dark_sea_green",          0xFF8FBC8F},
    {"dark_slate_blue",         0xFF483D8B},
    {"dark_slate_gray",         0xFF2F4F4F},
    {"dark_turquoise",          0xFF00CED1},
    {"dark_violet",             0xFF9400D3},
    {"deep_pink",               0xFFFF1493},
    {"deep_sky_blue",           0xFF00BFFF},
    {"dim_gray",                0xFF696969},
    {"dodger_blue",             0xFF1E90FF},
    {"fire_brick",              0xFFB22222},
    {"floral_white",            0xFFFFFAF0},
    {"forest_green",            0xFF228B22},
    {"fuchsia",                 0xFFFF00FF},
    {"gainsboro",               0xFFDCDCDC},
    {"ghost_white",             0xFFF8F8FF},
    {"gold",                    0xFFFFD700},
    {"golden_rod",              0xFFDAA520},
    {"gray",                    0xFF808080},
    {"green",                   0xFF008000},
    {"green_yellow",            0xFFADFF2F},
    {"honey_dew",               0xFFF0FFF0},
    {"hot_pink",                0xFFFF69B4},
    {"indian_red",              0xFFCD5C5C},
    {"indigo",                  0xFF4B0082},
    {"ivory",                   0xFFFFFFF0},
    {"khaki",                   0xFFF0E68C},
    {"lavender",                0xFFE6E6FA},
    {"lavender_blush",          0xFFFFF0F5},
    {"lawn_green",              0xFF7CFC00},
    {"lemon_chiffon",           0xFFFFFACD},
    {"light_blue",              0xFFADD8E6},
    {"light_coral",             0xFFF08080},
    {"light_cyan",              0xFFE0FFFF},
    {"light_golden_rod_yellow", 0xFFFAFAD2},
    {"light_gray",              0xFFD3D3D3},
    {"light_green",             0xFF90EE90},
    {"light_pink",              0xFFFFB6C1},
    {"light_salmon",            0xFFFFA07A},
    {"light_sea_green",         0xFF20B2AA},
    {"light_sky_blue",          0xFF87CEFA},
    {"light_slate_gray",        0xFF778899},
    {"light_steel_blue",        0xFFB0C4DE},
    {"light_yellow",            0xFFFFFFE0},
    {"lime",                    0xFF00FF00},
    {"lime_green",              0xFF32CD32},
    {"linen",                   0xFFFAF0E6},
    {"magenta",                 0xFFFF00FF},
    {"maroon",                  0xFF800000},
    {"medium_aqua_marine",      0xFF66CDAA},
    {"medium_blue",             0xFF0000CD},
    {"medium_orchid",           0xFFBA55D3},
    {"medium_purple",           0xFF9370DB},
    {"medium_sea_green",        0xFF3CB371},
    {"medium_slate_blue",       0xFF7B68EE},
    {"medium_spring_green",     0xFF00FA9A},
    {"medium_turquoise",        0xFF48D1CC},
    {"medium_violet_red",       0xFFC71585},
    {"midnight_blue",           0xFF191970},
    {"mint_cream",              0xFFF5FFFA},
    {"misty_rose",              0xFFFFE4E1},
    {"moccasin",                0xFFFFE4B5},
    {"navajo_white",            0xFFFFDEAD},
    {"navy",                    0xFF000080},
    {"old_lace",                0xFFFDF5E6},
    {"olive",                   0xFF808000},
    {"olive_drab",              0xFF6B8E23},
    {"orange",                  0xFFFFA500},
    {"orange_red",              0xFFFF4500},
    {"orchid",                  0xFFDA70D6},
    {"pale_golden_rod",         0xFFEEE8AA},
    {"pale_green",              0xFF98FB98},
    {"pale_turquoise",          0xFFAFEEEE},
    {"pale_violet_red",         0xFFDB7093},
    {"papaya_whip",             0xFFFFEFD5},
    {"peach_puff",              0xFFFFDAB9},
    {"peru",                    0xFFCD853F},
    {"pink",                    0xFFFFC0CB},
    {"plum",                    0xFFDDA0DD},
    {"powder_blue",             0xFFB0E0E6},
    {"purple",                  0xFF800080},
    {"rebecca_purple",          0xFF663399},
    {"red",                     0xFFFF0000},
    {"rosy_brown",              0xFFBC8F8F},
    {"royal_blue",              0xFF4169E1},
    {"saddle_brown",            0xFF8B4513},
    {"salmon",                  0xFFFA8072},
    {"sandy_brown",             0xFFF4A460},
    {"sea_green",               0xFF2E8B57},
    {"sea_shell",               0xFFFFF5EE},
    {"sienna",                  0xFFA0522D},
    {"silver",                  0xFFC0C0C0},
    {"sky_blue",                0xFF87CEEB},
    {"slate_blue",              0xFF6A5ACD},
    {"slate_gray",              0xFF708090},
    {"snow",                    0xFFFFFAFA},
    {"spring_green",            0xFF00FF7F},
    {"steel_blue",              0xFF4682B4},
    {"tan",                     0xFFD2B48C},
    {"teal",                    0xFF008080},
    {"thistle",                 0xFFD8BFD8},
    {"tomato",                  0xFFFF6347},
    {"turquoise",               0xFF40E0D0},
    {"violet",                  0xFFEE82EE},
    {"wheat",                   0xFFF5DEB3},
    {"white",                   0xFFFFFFFF},
    {"white_smoke",             0xFFF5F5F5},
    {"yellow",                  0xFFFFFF00},
    {"yellow_green",            0xFF9ACD32}
};

/* 函数声明 */
/* 画布设置(像素尺寸) */
void turtle_init(void *, int, int, const char *, turtle_save_type_t, turtle_canvas_type_t);
/* TODO: 画布归一化尺寸设置(0.0～1.0) */
/* TODO: 画布尺寸设置与获取 */

/* 全局控制 */
void turtle_reset(void *);
void turtle_push(void *);
void turtle_pop(void *);
void turtle_save_to_file(void *);

/* 画笔设置 */
/* TODO: set_color，同时设置画笔和填充色 */
void turtle_set_line_width(void *, double);
double turtle_get_line_width(void *);
/* 通过RGB分量值设置颜色 */
void turtle_set_pen_color_rgb(void *, unsigned char,
                       unsigned char, unsigned char);
/* 通过RGBA分量值设置颜色 */
void turtle_set_pen_color_rgba(void *, unsigned char,
        unsigned char, unsigned char, unsigned char);
/* 通过24位十六进制颜色值设置颜色 */
void turtle_set_pen_color_bit32(void *, unsigned int);
/* 通过颜色名称设置颜色 */
void turtle_set_pen_color_name_string(void *, char *);
/* 通过颜色枚举常量设置颜色 */
void turtle_set_pen_color_name_enum(void *, color_name_t);

/* 通过RGB分量值设置颜色 */
void turtle_set_fill_color_rgb(void *, unsigned char,
                       unsigned char, unsigned char);
/* 通过RGBA分量值设置颜色 */
void turtle_set_fill_color_rgba(void *, unsigned char,
        unsigned char, unsigned char, unsigned char);
/* 通过24位十六进制颜色值设置颜色 */
void turtle_set_fill_color_bit32(void *, unsigned int);
/* 通过颜色名称设置颜色 */
void turtle_set_fill_color_name_string(void *, char *);
/* 通过颜色枚举常量设置颜色 */
void turtle_set_fill_color_name_enum(void *, color_name_t);

/* 画笔运动(绘图) */
void turtle_pen_up(void *);
void turtle_pen_down(void *);
void turtle_forward(void *, double);
void turtle_backward(void *, double);
void turtle_turn_left(void *, double);
void turtle_turn_right(void *, double);
void turtle_goto(void *, double, double);
void turtle_arc(void *, double, double, int);
void turtle_dot(void *);
void turtle_strafe_left(void *, double);
void turtle_strafe_right(void *, double);
void turtle_put_str(void *, char *, double, char *);

/* 填充控制 */
void turtle_begin_fill(void *);
void turtle_end_fill(void *);

/* 位姿控制 */
void turtle_crawl(void *, double);
void turtle_set_heading(void *, double);
void turtle_set_home(void *);
void turtle_set_x(void *, double);
void turtle_set_y(void *, double);
double turtle_get_x(void *);
double turtle_get_y(void *);
void turtle_draw_turtle(void *);

/* 内部操作 */
void _turtle_set_pen_colors_rgba(turtle *, unsigned char,
           unsigned char, unsigned char, unsigned char);
void _turtle_set_pen_colors_bit32(turtle *, unsigned int);
void _turtle_set_fill_colors_rgba(turtle *, unsigned char,
           unsigned char, unsigned char, unsigned char);
void _turtle_set_fill_colors_bit32(turtle *, unsigned int);
void _turtle_rotate(turtle *, double);
void _turtle_goto(turtle *, double, double);
void _turtle_draw_line(turtle *, double, double, double, double);
double _turtle_get_pixel_unit(turtle *);
double _turtle_get_pixel_unit_x(turtle *);
double _turtle_get_pixel_unit_y(turtle *);
void _turtle_draw_circle(turtle *, int, int, int);
void _turtle_fill_circle(turtle *, int, int, int);
void _turtle_fill_circle_here(turtle *, int);

/*-----------------------------------------------------------------------
// 名称: turtle * turtle_ctor(turtle *this)
// 功能: 构造turtle龟行协议类对象
// 算法: 主要是对各函数指针按需进行赋值
// 参数:
//       [turtle *this] --- 龟行协议类对象指针
// 返回: [turtle *] --- 该协议类对象
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
turtle * turtle_ctor(turtle *this)
{
    /* 系统初始化 */
    this->init         = turtle_init;
    /* 系统复位 */
    this->reset        = turtle_reset;
    /* 乌龟状态入栈 */
    this->push         = turtle_push;
    /* 乌龟状态出栈 */
    this->pop          = turtle_pop;
    /* 保存绘图结果到指定文件 */
    this->save         = turtle_save_to_file;

    /* 设置画笔宽度 */
    this->setlinewidth = turtle_set_line_width;
    this->pensize      = turtle_set_line_width;
    this->penwidth     = turtle_set_line_width;
    /* 获取画笔宽度 */
    this->getlinewidth = turtle_get_line_width;
    this->getpensize   = turtle_get_line_width;
    this->getpenwidth  = turtle_get_line_width;

    /* 设置画笔颜色 */
    this->setpencolor  = turtle_set_pen_color_bit32;
    this->penrgb       = turtle_set_pen_color_rgb;
    this->penrgba      = turtle_set_pen_color_rgba;
    this->penbit32     = turtle_set_pen_color_bit32;
    this->penname      = turtle_set_pen_color_name_string;
    this->penenum      = turtle_set_pen_color_name_enum;

    /* 设置填充颜色 */
    this->setfillcolor = turtle_set_fill_color_bit32;
    this->fillrgb      = turtle_set_fill_color_rgb;
    this->fillrgba     = turtle_set_fill_color_rgba;
    this->fillbit32    = turtle_set_fill_color_bit32;
    this->fillname     = turtle_set_fill_color_name_string;
    this->fillenum     = turtle_set_fill_color_name_enum;

    /* 提起画笔 */
    this->penup        = turtle_pen_up;
    this->up           = turtle_pen_up;
    this->pu           = turtle_pen_up;
    /* 落下画笔 */
    this->pendown      = turtle_pen_down;
    this->down         = turtle_pen_down;
    this->pd           = turtle_pen_down;

    /* 向前爬行 */
    this->forward      = turtle_forward;
    this->fd           = turtle_forward;
    /* 向后爬行 */
    this->backward     = turtle_backward;
    this->back         = turtle_backward;
    this->bk           = turtle_backward;

    /* 向左转 */
    this->left         = turtle_turn_left;
    this->lt           = turtle_turn_left;
    /* 向右转 */
    this->right        = turtle_turn_right;
    this->rt           = turtle_turn_right;

    /* 按当前方向爬行 */
    this->go           = turtle_crawl;
    /* 移动到绝对坐标 */
    this->to           = turtle_goto;

    /* 绘制圆弧 */
    this->arc          = turtle_arc;
    this->circle       = turtle_arc;

    /* 绘制圆点 */
    this->dot          = turtle_dot;

    /* 向左扫射 */
    this->strafeleft   = turtle_strafe_left;
    this->sl           = turtle_strafe_left;
    /* 向右扫射 */
    this->straferight  = turtle_strafe_left;
    this->sr           = turtle_strafe_left;

    /* 在当前位置输出一个字符串 */
    this->putstr       = turtle_put_str;

    /* 开始填充 */
    this->beginfill    = turtle_begin_fill;
    this->bf           = turtle_begin_fill;
    /* 结束填充 */
    this->endfill      = turtle_end_fill;
    this->ef           = turtle_begin_fill;

    /* 设置乌龟方向 */
    this->setheading   = turtle_set_heading;
    this->seth         = turtle_set_heading;

    /* 设置乌龟到原点并指向东 */
    this->sethome      = turtle_set_home;
    this->home         = turtle_set_home;

    /* 设置乌龟x坐标 */
    this->setx         = turtle_set_x;
    this->sx           = turtle_set_x;
    /* 设置乌龟y坐标 */
    this->sety         = turtle_set_y;
    this->sy           = turtle_set_y;

    /* 获取乌龟x坐标 */
    this->getx         = turtle_get_x;
    this->gx           = turtle_get_x;
    /* 获取乌龟y坐标 */
    this->gety         = turtle_get_y;
    this->gy           = turtle_get_y;

    /* 绘制乌龟标志 */
    this->logo         = turtle_draw_turtle;

    return this;
}

/*-----------------------------------------------------------------------
// 名称: void turtle_dtor(turtle *this)
// 功能: 析构turtle龟行协议类对象
// 算法: 调用turtle_cleanup函数主要是对各函数指针按需进行赋值
// 参数:
//       [turtle *this] --- 龟行协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void turtle_dtor(turtle *this)
{
    /* 释放图像资源 */
    cairo_destroy(this->cr);
    cairo_surface_destroy(this->surface);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_init(void *this, int width, int height, const char *filename,
//                 turtle_save_type_t save_type, turtle_canvas_type_t canvas_type)
// 功能: 初始化乌龟状态并创建Cairo绘图环境
// 参数:
//       [void *this] ----------------------- 龟行协议类对象指针
//       [int width] ------------------------ 绘图环境宽度(像素单位)
//       [int height] ----------------------- 绘图环境高度(像素单位)
//       [const char *filename] ------------- 保存绘图结果的文件名(需要带有后缀名)
//       [turtle_save_type_t save_type] ----- 绘图类型(PNG、SVG和PDF)枚举常量
//       [turtle_save_type_t canvas_type] --- 画布坐标类型(像素坐标/归一化坐标)枚举常量
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void turtle_init(void *this, int width, int height, const char *filename,
          turtle_save_type_t saved_type, turtle_canvas_type_t canvas_type)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    /* 记录绘图输出方式 */
    This->saved_filename = filename;
    This->saved_type = saved_type;
    This->canvas_type = canvas_type;

    /* 创建Cairo绘图环境 */
    switch(saved_type)
    {
    case TURTLE_OUT_TYPE_PNG:
        This->surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
        break;
    case TURTLE_OUT_TYPE_SVG:
        This->surface = cairo_svg_surface_create(filename, width, height);
        break;
    case TURTLE_OUT_TYPE_PDF:
        This->surface = cairo_pdf_surface_create(filename, width, height);
        break;
    default:
        This->surface = cairo_pdf_surface_create(filename, width, height);
        break;
    }
    This->cr = cairo_create(This->surface);

    /* 保存图像宽和高(像素单位) */
    This->main_field_width = width;
    This->main_field_height = height;

    /* 复位乌龟状态 */
    turtle_reset(this);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_reset(void *this)
// 功能: 乌龟状态复位
// 参数:
//       [void *this] --- 龟行协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void turtle_reset(void *this)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    /* 将海龟置于图像中心 */
    This->main_turtle.xpos = This->main_field_width  / 2.0;
    This->main_turtle.ypos = This->main_field_height / 2.0;

    /* 海龟方向向右 */
    This->main_turtle.heading = 0.0;

    /* 画笔宽度 */
    This->main_turtle.line_width = 1.0;

    /* 画笔颜色为黑色 */
    This->main_turtle.pen_color.red   = 0;
    This->main_turtle.pen_color.green = 0;
    This->main_turtle.pen_color.blue  = 0;
    This->main_turtle.pen_color.alpha = 0xFF;

    /* 填充颜色为黑色 */
    This->main_turtle.fill_color.red   = 0;
    This->main_turtle.fill_color.green = 0;
    This->main_turtle.fill_color.blue  = 0;
    This->main_turtle.fill_color.alpha = 0xFF;

    /* 画笔状态为按下 */
    This->main_turtle.pendown = true;

    /* 关闭填充状态 */
    This->main_turtle.filled = false;
}

/*-----------------------------------------------------------------------
// 名称: void turtle_push(void *this)
// 功能: 乌龟状态入栈
// 参数:
//       [void *this] --- 龟行协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void turtle_push(void *this)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    push(This->main_turtle);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_push(void *this)
// 功能: 乌龟状态出栈
// 参数:
//       [void *this] --- 龟行协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void turtle_pop(void *this)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    This->main_turtle = pop();
}

/*-----------------------------------------------------------------------
// 名称: void turtle_save_to_file(void *this)
// 功能：将绘制结果输出到初始化函数中指定的文件名及类型的文件
// 参数:
//       [void *this] --- 龟行协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void turtle_save_to_file(void *this)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    /* 将Memory的渲染效果存储到PDF文件中 */
    switch(This->saved_type)
    {
    case TURTLE_OUT_TYPE_PNG:
        cairo_surface_write_to_png(This->surface, This->saved_filename);
        break;
    case TURTLE_OUT_TYPE_SVG:
        break;
    case TURTLE_OUT_TYPE_PDF:
        cairo_show_page(This->cr);
        break;
    default:
        cairo_surface_write_to_png(This->surface, "temp.png");
        break;
    }
}

/*-----------------------------------------------------------------------
// 名称: void turtle_set_line_width(void *this, double line_width)
// 功能: 设置绘制线宽
// 参数:
//       [void *this] ---------- 龟行协议类对象指针
//       [double line_width] --- 绘制线宽
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void turtle_set_line_width(void *this, double line_width)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    /* 根据画布单位模式设置 */
    switch(This->canvas_type)
    {
    case TURTLE_CANVAS_NORM:
        line_width *= _turtle_get_pixel_unit(This);
        break;
    default:
        break;
    }

    This->main_turtle.line_width = line_width;
}

/*-----------------------------------------------------------------------
// 名称: double turtle_get_line_width(void *this)
// 功能: 设置绘制线宽
// 参数:
//       [void *this] --- 龟行协议类对象指针
// 返回: [double] ------- 当前绘制线宽
// 作者: 耿楠
// 日期: 2022年11月15日
//-----------------------------------------------------------------------*/
double turtle_get_line_width(void *this)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    /* 根据画布单位模式设置 */
    switch(This->canvas_type)
    {
    case TURTLE_CANVAS_NORM:
        return This->main_turtle.line_width / _turtle_get_pixel_unit(This);
        break;
    default:
        break;
    }

    return This->main_turtle.line_width;
}

/*-----------------------------------------------------------------------
// 名称: void turtle_set_pen_color_rgb(void *this, unsigned char r,
//                                unsigned char g, unsigned char b);
// 功能: 通过RGB分量值设置画笔颜色，将alpha分量赋值为0
// 参数:
//       [void *this] -----==---- 龟行协议类对象指针
//       [unsigned char r] --- 颜色的红色分量值[0, 255]
//       [unsigned char g] --- 颜色的绿色分量值[0, 255]
//       [unsigned char b] --- 颜色的蓝色分量值[0, 255]
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_set_pen_color_rgb(void *this, unsigned char r,
                          unsigned char g, unsigned char b)
{
    _turtle_set_pen_colors_rgba(this, r, g, b, 0xFF);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_set_pen_color_rgba(void *this, unsigned char r,
//               unsigned char g, unsigned char b, unsigned char a);
// 功能: 通过RGBA分量值设置画笔颜色
// 参数:
//       [void *this] -----==---- 龟行协议类对象指针
//       [unsigned char r] --- 颜色的红色分量值[0, 255]
//       [unsigned char g] --- 颜色的绿色分量值[0, 255]
//       [unsigned char b] --- 颜色的蓝色分量值[0, 255]
//       [unsigned char a] --- 颜色的alpha分量值[0, 255]
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_set_pen_color_rgba(void *this, unsigned char r,
        unsigned char g, unsigned char b, unsigned char a)
{
    _turtle_set_pen_colors_rgba(this, r, g, b, a);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_set_pen_color_bit32(void *this, unsigned int color)
// 功能: 通过24位十六进制颜色值设置画笔颜色
// 参数:
//       [void *this] -----==---- 龟行协议类对象指针
//       [unsigned int color] --- 24位十六进制颜色值
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_set_pen_color_bit32(void *this, unsigned int color)
{
    _turtle_set_pen_colors_bit32(this, color);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_set_pen_color_name_string(void *this, char *color_name)
// 功能: 通过颜色名称字符串设置画笔颜色
// 参数:
//       [void *this] --------- 龟行协议类对象指针
//       [char *color_name] --- 颜色名称字符串
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_set_pen_color_name_string(void *this, char *color_name)
{
    int i;
    unsigned int color;
    size_t color_sets_len = sizeof(color_sets) / sizeof(color_sets[0]);

    for(i = 0; i < color_sets_len; i++)
    {
        if(strcmp(color_name, color_sets[i].color_name) == 0)
        {
            color = color_sets[i].color_value;
            break;
        }
    }

    if(i == color_sets_len)
    {
        color = 0;
    }

    _turtle_set_pen_colors_bit32(this, color);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_set_pen_color_name_enum(void *this, color_name_t color_name)
// 功能: 通过颜色名称字符串设置画笔颜色
// 参数:
//       [void *this] ---------------- 龟行协议类对象指针
//       [color_name_t color_name] --- 颜色名称枚举常量
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_set_pen_color_name_enum(void *this, color_name_t color_name)
{
    unsigned int color;

    if(color_name >= TURTLE_COLOR_NUMS || color_name < 0)
    {
        color = 0;
    }
    else
    {
        color = color_sets[color_name].color_value;
    }

    _turtle_set_pen_colors_bit32(this, color);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_set_fill_color_rgb(void *this, unsigned char r,
//                                unsigned char g, unsigned char b);
// 功能: 通过RGB分量值设置填充颜色，将alpha分量赋值为0
// 参数:
//       [void *this] -----==---- 龟行协议类对象指针
//       [unsigned char r] --- 颜色的红色分量值[0, 255]
//       [unsigned char g] --- 颜色的绿色分量值[0, 255]
//       [unsigned char b] --- 颜色的蓝色分量值[0, 255]
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_set_fill_color_rgb(void *this, unsigned char r,
                          unsigned char g, unsigned char b)
{
    _turtle_set_fill_colors_rgba(this, r, g, b, 0xFF);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_set_fill_color_rgba(void *this, unsigned char r,
//               unsigned char g, unsigned char b, unsigned char a);
// 功能: 通过RGBA分量值设置填充颜色
// 参数:
//       [void *this] -----==---- 龟行协议类对象指针
//       [unsigned char r] --- 颜色的红色分量值[0, 255]
//       [unsigned char g] --- 颜色的绿色分量值[0, 255]
//       [unsigned char b] --- 颜色的蓝色分量值[0, 255]
//       [unsigned char a] --- 颜色的alpha分量值[0, 255]
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_set_fill_color_rgba(void *this, unsigned char r,
        unsigned char g, unsigned char b, unsigned char a)
{
    _turtle_set_fill_colors_rgba(this, r, g, b, a);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_set_fill_color_bit32(void *this, unsigned int color)
// 功能: 通过24位十六进制颜色值设置填充颜色
// 参数:
//       [void *this] -----==---- 龟行协议类对象指针
//       [unsigned int color] --- 24位十六进制颜色值
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_set_fill_color_bit32(void *this, unsigned int color)
{
    _turtle_set_fill_colors_bit32(this, color);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_set_fill_color_name_string(void *this, char *color_name)
// 功能: 通过颜色名称字符串设置填充颜色
// 参数:
//       [void *this] --------- 龟行协议类对象指针
//       [char *color_name] --- 颜色名称字符串
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_set_fill_color_name_string(void *this, char *color_name)
{
    int i;
    unsigned int color;
    size_t color_sets_len = sizeof(color_sets) / sizeof(color_sets[0]);

    for(i = 0; i < color_sets_len; i++)
    {
        if(strcmp(color_name, color_sets[i].color_name) == 0)
        {
            color = color_sets[i].color_value;
            break;
        }
    }

    if(i == color_sets_len)
    {
        color = 0;
    }

    _turtle_set_fill_colors_bit32(this, color);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_set_fill_color_name_enum(void *this, color_name_t color_name)
// 功能: 通过颜色名称字符串设置填充颜色
// 参数:
//       [void *this] ---------------- 龟行协议类对象指针
//       [color_name_t color_name] --- 颜色名称枚举常量
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_set_fill_color_name_enum(void *this, color_name_t color_name)
{
    unsigned int color;

    if(color_name >= TURTLE_COLOR_NUMS || color_name < 0)
    {
        color = 0;
    }
    else
    {
        color = color_sets[color_name].color_value;
    }

    _turtle_set_fill_colors_bit32(this, color);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_pen_up(void *this)
// 功能: 提笔(取消绘制)
// 参数:
//       [void *this] ----- 龟行协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void turtle_pen_up(void *this)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    This->main_turtle.pendown = false;
}

/*-----------------------------------------------------------------------
// 名称: void turtle_pen_down(void *this)
// 功能: 落笔(开始绘制)
// 参数:
//       [void *this] ----- 龟行协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void turtle_pen_down(void *this)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    This->main_turtle.pendown = true;
}

/*-----------------------------------------------------------------------
// 名称: void turtle_forward(void *this, double distance)
// 功能: 向前爬行指定距离
// 参数:
//       [void *this] --- 龟行协议类对象指针
//       [double distance] --- 爬行距离(像素单位)
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_forward(void *this, double distance)
{
    turtle_crawl(this, distance);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_backward(void *this, double distance)
// 功能: 向后爬行指定距离
// 参数:
//       [void *this] -------- 龟行协议类对象指针
//       [double distance] --- 爬行距离
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_backward(void *this, double distance)
{
    turtle_crawl(this, -distance);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_turn_left(void *this, double angle)
// 功能: 左转指定的角度
// 算法: 累加后需要确保处理[0,360]之间。
// 参数:
//       [void *this] ----- 龟行协议类对象指针
//       [double angle] --- 旋转角度(度)
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_turn_left(void *this, double angle)
{
    _turtle_rotate(this, angle);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_turn_right(void *this, double angle)
// 功能: 右转指定的角度
// 算法: 累加后需要确保处理[0,360]之间。
// 参数:
//       [void *this] ----- 龟行协议类对象指针
//       [double angle] --- 旋转角度(度)
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_turn_right(void *this, double angle)
{
    _turtle_rotate(this, -angle);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_goto(void *this, double x, double y)
// 功能: 将乌龟移动到指定位置
// 算法：通过调用_turtle_goto函数实现
// 参数:
//       [void *this] --- 龟行协议类对象指针
//       [double x] ----- x方向移动距离
//       [double y] ----- y方向移动距离
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void turtle_goto(void *this, double x, double y)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    /* 根据画布单位模式设置 */
    switch(This->canvas_type)
    {
    case TURTLE_CANVAS_NORM:
        x *= _turtle_get_pixel_unit(This);
        y *= _turtle_get_pixel_unit(This);
        break;
    default:
        break;
    }

    _turtle_goto(This, (This->main_field_width / 2 + x),
                           (This->main_field_height / 2 - y));
}

/*-----------------------------------------------------------------------
// 名称: void turtle_arc(void *this, double radius, double angle, int steps)
// 功能: 绘制指定半径、角度及分段数的圆弧
// 算法：通过线段近似逼近，当angle为0时取360度，当steps为0时，根据angle
//       计算比例得到。
// 参数:
//       [void *this] -------- 龟行协议类对象指针
//       [double radius] ----- 圆弧半径
//       [double angle] ------ 圆弧转角
//       [int steps] --------- 圆弧分段数
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月09日
//-----------------------------------------------------------------------*/
void turtle_arc(void *this, double radius, double angle, int steps)
{
    int i;
    double w, w2, l;
    double rad = fabs(radius);
    double frac;

    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    /* 根据画布单位模式设置 */
    switch(This->canvas_type)
    {
    case TURTLE_CANVAS_NORM:
        rad *= _turtle_get_pixel_unit(This);
        break;
    default:
        break;
    }

    if(fabs(angle) < 1e-6)
    {
        angle = 360.0;
    }

    if(steps <= 0)
    {
        frac = fabs(angle) / 360.0;
        /* 正六边形边长等于半径，做为最小的圆多边形逼近 */
        /* 加35，表示最小逼近为正36边形 */
        /* 最大逼近多边形为正60边形 */
        steps = 1 + (int)(MIN((35 + rad / 6.0), 59.0) * frac);
    }

    w = angle / steps;
    w2 = 0.5 * w;
    l = 2.0 * radius * sin(w2 * PI / 180);

    if(radius < 0)
    {
        w = -w;
        w2 = -w2;
        l = -l;
    }

    _turtle_rotate(this, w2);
    for(i = 0; i < steps; i++)
    {
        turtle_crawl(this, l);
        _turtle_rotate(this, w);
    }
    _turtle_rotate(this, -w2);

    if(angle < 0)
    {
        _turtle_rotate(this, 180);
    }
}

/*-----------------------------------------------------------------------
// 名称: void turtle_dot(void *this)
// 功能: 在当前位置绘制一个点
// 算法：绘制一个极短的直线
// 参数:
//       [void *this] --- 龟行协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void turtle_dot(void *this)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    cairo_set_source_rgba(This->cr, This->main_turtle.pen_color.red / 255.0,
                              This->main_turtle.pen_color.green / 255.0,
                              This->main_turtle.pen_color.blue / 255.0,
                              This->main_turtle.pen_color.alpha / 255.0);
    cairo_set_line_width(This->cr, This->main_turtle.line_width);
    cairo_move_to(This->cr, This->main_turtle.xpos, This->main_turtle.ypos);
    cairo_line_to(This->cr, This->main_turtle.xpos, This->main_turtle.ypos);
    cairo_stroke_preserve(This->cr);
    cairo_set_line_cap(This->cr, CAIRO_LINE_CAP_ROUND);
    cairo_set_line_join(This->cr, CAIRO_LINE_JOIN_ROUND);
    cairo_stroke(This->cr);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_strafe_left(void *this, double distance)
// 功能: 向左扫射-->左转90度后，向前爬行指定距离，然后转回原方向
// 算法: 通过调用turtle_turn_left、turtle_forward和turtle_turn_right函数实现
// 参数:
//       [void *this] -------- 龟行协议类对象指针
//       [double distance] --- 爬行距离
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_strafe_left(void *this, double distance)
{
    turtle_turn_left(this, 90);
    turtle_forward(this, distance);
    turtle_turn_right(this, 90);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_strafe_right(void *this, double distance)
// 功能: 向右扫射-->右转90度后，向前爬行指定距离，然后转回原方向
// 算法: 通过调用turtle_turn_right、turtle_forward和turtle_turn_left函数实现
// 参数:
//       [void *this] -------- 龟行协议类对象指针
//       [double distance] --- 爬行距离
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_strafe_right(void *this, double distance)
{
    turtle_turn_right(this, 90);
    turtle_forward(this, distance);
    turtle_turn_left(this, 90);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_put_str(void *this, char *str, double fontsize, char *font)
// 功能：在乌龟当前位置用指定的字体字号输出一个字串
// 参数:
//       [void *this] -------- 龟行协议类对象指针
//       [char *str] --------- 需要输出的字符串
//       [double fontsize] --- 字号
//       [char *str] --------- 字体名称
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月07日
//-----------------------------------------------------------------------*/
void turtle_put_str(void *this, char *str, double fontsize, char *font)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    double rad = This->main_turtle.heading * PI / 180.0;

    cairo_select_font_face (This->cr, font, CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size (This->cr, fontsize);
    cairo_set_source_rgba (This->cr, This->main_turtle.pen_color.red / 255.0,
                              This->main_turtle.pen_color.green / 255.0,
                              This->main_turtle.pen_color.blue / 255.0,
                              This->main_turtle.pen_color.alpha / 255.0);
    cairo_move_to (This->cr, This->main_turtle.xpos, This->main_turtle.ypos);
    cairo_rotate (This->cr, -rad);
    cairo_show_text (This->cr, str);
    cairo_rotate (This->cr, rad);
    /* 绘图 */
    cairo_set_line_cap(This->cr, CAIRO_LINE_CAP_ROUND);
    cairo_set_line_join(This->cr, CAIRO_LINE_JOIN_ROUND);
    cairo_stroke(This->cr);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_begin_fill(void *this)
// 功能: 设置填充开始状态
// 算法：设置乌龟状态，并将绘图环境调整为绘制闭合路径方式。
// 参数:
//       [void *this] ----- 龟行协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void turtle_begin_fill(void *this)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    This->main_turtle.filled = true;

    /* 设置颜色 */
    cairo_set_source_rgba (This->cr, This->main_turtle.fill_color.red / 255.0,
                              This->main_turtle.fill_color.green / 255.0,
                              This->main_turtle.fill_color.blue / 255.0,
                              This->main_turtle.fill_color.alpha / 255.0);

    /* 设置线宽 */
    cairo_set_line_width (This->cr, This->main_turtle.line_width);
    /* cairo_set_line_cap(This->cr, CAIRO_LINE_CAP_ROUND); */
    cairo_move_to(This->cr, This->main_turtle.xpos, This->main_turtle.ypos);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_end_fill(void *this)
// 功能: 设置填充结束状态
// 算法：设置乌龟状态，并在绘图环境中生成闭合路径，然后记录路径后，设置绘制
//       颜色与线宽，最后实现路径绘制和填充操作。
// 参数:
//       [void *this] ----- 龟行协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void turtle_end_fill(void *this)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    cairo_close_path(This->cr);
    cairo_fill_preserve(This->cr);
    /* 设置颜色 */
    cairo_set_source_rgba (This->cr, This->main_turtle.pen_color.red / 255.0,
                              This->main_turtle.pen_color.green / 255.0,
                              This->main_turtle.pen_color.blue / 255.0,
                              This->main_turtle.pen_color.alpha / 255.0);

    /* 设置线宽 */
    cairo_set_line_width (This->cr, This->main_turtle.line_width);
    cairo_set_line_cap(This->cr, CAIRO_LINE_CAP_ROUND);
    cairo_set_line_join(This->cr, CAIRO_LINE_JOIN_ROUND);
    cairo_stroke(This->cr);
    cairo_fill(This->cr);

    This->main_turtle.filled = false;
}

/*-----------------------------------------------------------------------
// 名称: void turtle_set_heading(void *this, double angle)
// 功能: 设置乌龟方向
// 参数:
//       [void *this] ----- 龟行协议类对象指针
//       [double angle] --- 旋转方向(度)
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void turtle_set_heading(void *this, double angle)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    This->main_turtle.heading = angle;
}

/*-----------------------------------------------------------------------
// 名称: void turtle_set_home(void *this)
// 功能：设置乌龟回到原始位置
// 参数:
//       [void *this] --- 龟行协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_set_home(void *this)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    This->main_turtle.xpos = 0.0;
    This->main_turtle.ypos = 0.0;
    This->main_turtle.heading = 0.0;
}

/*-----------------------------------------------------------------------
// 名称: void turtle_set_x(void *this, double x)
// 功能：设置乌龟x坐标
// 参数:
//       [void *this] --- 龟行协议类对象指针
//       [double x] ----- x方向坐标值
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_set_x(void *this, double x)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    /* 根据画布单位模式设置 */
    switch(This->canvas_type)
    {
    case TURTLE_CANVAS_NORM:
        x *= _turtle_get_pixel_unit(This);
        break;
    default:
        break;
    }

    This->main_turtle.xpos = x;
}

/*-----------------------------------------------------------------------
// 名称: void turtle_set_y(void *this, double y)
// 功能：设置乌龟y坐标
// 参数:
//       [void *this] --- 龟行协议类对象指针
//       [double y] ----- y方向坐标值
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_set_y(void *this, double y)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    /* 根据画布单位模式设置 */
    switch(This->canvas_type)
    {
    case TURTLE_CANVAS_NORM:
        y *= _turtle_get_pixel_unit(This);
        break;
    default:
        break;
    }

    This->main_turtle.ypos = y;
}

/*-----------------------------------------------------------------------
// 名称: double turtle_get_x(void *this)
// 功能：获取乌龟当前位置x坐标
// 参数:
//       [void *this] --- 龟行协议类对象指针
// 返回: [double] --- 乌龟当前位置的x坐标
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
double turtle_get_x(void *this)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    return This->main_turtle.xpos;
}

/*-----------------------------------------------------------------------
// 名称: double turtle_get_y(void *this)
// 功能：获取乌龟当前位置y坐标
// 参数:
//       [void *this] --- 龟行协议类对象指针
// 返回: [double] --- 乌龟当前位置的y坐标
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
double turtle_get_y(void *this)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    return This->main_turtle.ypos;
}

/*-----------------------------------------------------------------------
// 名称: void _turtle_set_pen_colors_rgba(turtle *this, unsigned char r,
//                   unsigned char g, unsigned char b, unsigned char a)
// 功能：通过R、G、B、A分量值设置画笔颜色
// 参数:
//       [turtle *this] ------ 龟行协议类对象指针
//       [unsigned char r] --- 红色分量[0, 255]
//       [unsigned char g] --- 绿色分量[0, 255]
//       [unsigned char b] --- 蓝色分量[0, 255]
//       [unsigned char a] --- alpha分量[0, 255]
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void _turtle_set_pen_colors_rgba(turtle *this, unsigned char r,
            unsigned char g, unsigned char b, unsigned char a)
{
    this->main_turtle.pen_color.red   = r;
    this->main_turtle.pen_color.green = g;
    this->main_turtle.pen_color.blue  = b;
    this->main_turtle.pen_color.alpha = a;
}

/*-----------------------------------------------------------------------
// 名称: void _turtle_set_pen_colors_bit32(turtle *this, unsigned int color)
// 功能：通过24位十六进制值设置画笔颜色
// 参数:
//       [turtle *this] --------- 龟行协议类对象指针
//       [unsigned int color] --- 24位十六进制表示的颜色值
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void _turtle_set_pen_colors_bit32(turtle *this, unsigned int color)
{
    this->main_turtle.pen_color.red   = GET_COLOR_RED(color);
    this->main_turtle.pen_color.green = GET_COLOR_GREEN(color);
    this->main_turtle.pen_color.blue  = GET_COLOR_BLUE(color);
    this->main_turtle.pen_color.alpha = GET_COLOR_ALPHA(color);
}

/*-----------------------------------------------------------------------
// 名称: void _turtle_set_fill_colors_rgba(turtle *this, unsigned char r,
//                   unsigned char g, unsigned char b, unsigned char a)
// 功能：通过R、G、B、A分量值设置画笔颜色
// 参数:
//       [turtle *this] ------ 龟行协议类对象指针
//       [unsigned char r] --- 红色分量[0, 255]
//       [unsigned char g] --- 绿色分量[0, 255]
//       [unsigned char b] --- 蓝色分量[0, 255]
//       [unsigned char a] --- alpha分量[0, 255]
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void _turtle_set_fill_colors_rgba(turtle *this, unsigned char r,
            unsigned char g, unsigned char b, unsigned char a)
{
    this->main_turtle.fill_color.red   = r;
    this->main_turtle.fill_color.green = g;
    this->main_turtle.fill_color.blue  = b;
    this->main_turtle.fill_color.alpha = a;
}

/*-----------------------------------------------------------------------
// 名称: void _turtle_set_fill_colors_bit32(turtle *this, unsigned int color)
// 功能：通过24位十六进制值设置填充颜色
// 参数:
//       [turtle *this] --------- 龟行协议类对象指针
//       [unsigned int color] --- 24位十六进制表示的颜色值
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void _turtle_set_fill_colors_bit32(turtle *this, unsigned int color)
{
    this->main_turtle.fill_color.red   = GET_COLOR_RED(color);
    this->main_turtle.fill_color.green = GET_COLOR_GREEN(color);
    this->main_turtle.fill_color.blue  = GET_COLOR_BLUE(color);
    this->main_turtle.fill_color.alpha = GET_COLOR_ALPHA(color);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_crawl(void *this, double distance)
// 功能: 爬行
// 算法: 根据当前方向计算出x，y方向上的爬行距离，
//       然后调用_turtle_goto函数实现具体操作
// 参数:
//       [void *this] -------- 龟行协议类对象指针
//       [double distance] --- 爬行距离
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void turtle_crawl(void *this, double distance)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    /* 计算方向向量 */
    double rad = This->main_turtle.heading * PI / 180.0;
    double dx = cos(rad) * distance;
    double dy = sin(rad) * distance;

    /* 根据画布单位模式设置 */
    switch(This->canvas_type)
    {
    case TURTLE_CANVAS_NORM:
        dx *= _turtle_get_pixel_unit(This);
        dy *= _turtle_get_pixel_unit(This);
        break;
    default:
        break;
    }

    /* 移动到指定位置 */
    _turtle_goto(This, This->main_turtle.xpos + dx, This->main_turtle.ypos - dy);
}

/*-----------------------------------------------------------------------
// 名称: void _turtle_rotate(turtle *this, double angle)
// 功能: 旋转指定角度
// 参数:
//       [turtle *this] --- 龟行协议类对象指针
//       [double angle] --- 旋转角度
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月06日
//-----------------------------------------------------------------------*/
void _turtle_rotate(turtle *this, double angle)
{
    /* 乌龟方向调整 */
    this->main_turtle.heading += angle;

    /* 确保角度范围在[0.0, 360.0)之间 */
    if (this->main_turtle.heading < 0.0)
    {
        this->main_turtle.heading += 360.0;
    }
    else if (this->main_turtle.heading >= 360.0)
    {
        this->main_turtle.heading -= 360.0;
    }
}

/*-----------------------------------------------------------------------
// 名称: void _turtle_goto(turtle *this, double x, double y)
// 功能: 实现乌龟的移动操作
// 算法：根据抬起与按下状态，决定移动过程中是否进行绘制
// 参数:
//       [turtle *this] --- 龟行协议类对象指针
//       [double x] ------- x方向移动距离(像素单位)
//       [bouble y] ------- y方向移动距离(像素单位)
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void _turtle_goto(turtle *this, double x, double y)
{
    /* 如果画笔按下，则绘制直线 */
    if (this->main_turtle.pendown)
    {
        _turtle_draw_line(this, this->main_turtle.xpos,
                                this->main_turtle.ypos,
                                x, y);
    }

    /* 更新乌龟当前位置 */
    this->main_turtle.xpos = x;
    this->main_turtle.ypos = y;
}

/*-----------------------------------------------------------------------
// 名称: void _turtle_draw_line(turtle *this, double x0, double y0, double x1, double y1)
// 功能: 绘制一条直线
// 算法：如果处理填充状态则采用增量绘制模式绘制一个相对长度线段，
//       如果不处理填充状态，则移动到起点后直接绘制到终点。
// 参数:
//       [turtle *this] --- 龟行协议类对象指针
//       [double x0] ------ 起点坐标x分量
//       [double y0] ------ 起点坐标y分量
//       [double x1] ------ 终点坐标x分量
//       [double y1] ------ 终点坐标y分量
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void _turtle_draw_line(turtle *this, double x0, double y0, double x1, double y1)
{
    if (this->main_turtle.filled)
    {
        /* 绘制线段 */
        cairo_rel_line_to(this->cr, x1 - x0, y1 - y0);
    }
    else
    {
        /* 设置颜色 */
        cairo_set_source_rgba (this->cr, this->main_turtle.pen_color.red / 255.0,
                                  this->main_turtle.pen_color.green / 255.0,
                                  this->main_turtle.pen_color.blue / 255.0,
                                  this->main_turtle.pen_color.alpha / 255.0);

        /* 设置线宽 */
        cairo_set_line_width (this->cr, this->main_turtle.line_width);
        /* cairo_set_line_cap(this->cr, CAIRO_LINE_CAP_ROUND); */
        /* 移动画笔 */
        cairo_move_to(this->cr, x0, y0);
        /* 绘制线段 */
        cairo_line_to(this->cr, x1, y1);
        /* 绘图 */
        cairo_set_line_cap(this->cr, CAIRO_LINE_CAP_ROUND);
        cairo_set_line_join(this->cr, CAIRO_LINE_JOIN_ROUND);
        cairo_stroke(this->cr);
    }
}

/*-----------------------------------------------------------------------
// 名称: double _turtle_get_pixel_unit(turtle *this)
// 功能: 返回归一化坐标下，x,y方向中最大长度1个绘制单位占有的像素数
// 参数:
//       [turtle *this] --- 龟行协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月09日
//-----------------------------------------------------------------------*/
double _turtle_get_pixel_unit(turtle *this)
{
    /* 归一化坐标下，1个绘制单位占有的像素数 */
    if(this->main_field_width >= this->main_field_height)
    {
        return (this->main_field_height / 2.0);
    }
    else
    {
        return (this->main_field_width / 2.0);
    }
}

/*-----------------------------------------------------------------------
// 名称: double _turtle_get_pixel_unit_x(turtle *this)
// 功能: 返回归一化坐标下，x方向1个绘制单位占有的像素数
// 参数:
//       [turtle *this] --- 龟行协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月09日
//-----------------------------------------------------------------------*/
double _turtle_get_pixel_unit_x(turtle *this)
{
    return (this->main_field_width / 2.0);
}

/*-----------------------------------------------------------------------
// 名称: double _turtle_get_pixel_unit_y(turtle *this)
// 功能: 返回归一化坐标下，y方向1个绘制单位占有的像素数
// 参数:
//       [turtle *this] --- 龟行协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月09日
//-----------------------------------------------------------------------*/
double _turtle_get_pixel_unit_y(turtle *this)
{
    return (this->main_field_height / 2.0);
}

/*-----------------------------------------------------------------------
// 名称: void _turtle_draw_circle(turtle *this, int x0, int y0, int radius)
// 功能: 绘制一个圆
// 算法：直接调用Cairo库实现绘制
// 参数:
//       [turtle *this] --- 龟行协议类对象指针
//       [int x0] --------- 圆心坐标x分量
//       [int y0] --------- 圆心坐标y分量
//       [int radius] ----- 终点坐标y分量
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void _turtle_draw_circle(turtle *this, int x0, int y0, int radius)
{
    if (this->main_turtle.filled)
    {
        _turtle_fill_circle(this, x0, y0, radius);
    }

    cairo_set_source_rgba (this->cr, this->main_turtle.pen_color.red / 255.0,
                              this->main_turtle.pen_color.green / 255.0,
                              this->main_turtle.pen_color.blue / 255.0,
                              this->main_turtle.pen_color.alpha / 255.0);
    /* 设置线宽 */
    cairo_set_line_width (this->cr, this->main_turtle.line_width);
    cairo_arc(this->cr, x0, y0, radius, 0, 2 * PI);
    cairo_stroke_preserve(this->cr);
    cairo_set_line_cap(this->cr, CAIRO_LINE_CAP_ROUND);
    cairo_set_line_join(this->cr, CAIRO_LINE_JOIN_ROUND);
    /* 绘图 */
    cairo_stroke(this->cr);
}

/*-----------------------------------------------------------------------
// 名称: void _turtle_fill_circle(turtle *this, int x0, int y0, int radius)
// 功能: 绘制一个填充圆
// 算法：直接调用Cairo库实现绘制
// 参数:
//       [turtle *this] --- 龟行协议类对象指针
//       [int x0] --------- 圆心坐标x分量
//       [int y0] --------- 圆心坐标y分量
//       [int radius] ----- 终点坐标y分量
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void _turtle_fill_circle(turtle *this, int x0, int y0, int radius)
{
    cairo_set_source_rgba (this->cr, this->main_turtle.pen_color.red / 255.0,
                              this->main_turtle.pen_color.green / 255.0,
                              this->main_turtle.pen_color.blue / 255.0,
                              this->main_turtle.pen_color.alpha / 255.0);
    /* 设置线宽 */
    cairo_arc(this->cr, x0, y0, radius, 0, 2 * PI);
    cairo_fill_preserve(this->cr);
    cairo_set_line_join(this->cr, CAIRO_LINE_JOIN_ROUND);
    cairo_fill(this->cr);
}

/* 在当前位置绘制填充圆 */
void _turtle_fill_circle_here(turtle *this, int radius)
{
    _turtle_fill_circle(this, this->main_turtle.xpos, this->main_turtle.ypos, radius);
}

/*-----------------------------------------------------------------------
// 名称: void turtle_draw_turtle(void *this)
// 功能：绘制乌龟标志图形
// 算法：通过圆拼凑实现
// 参数:
//       [void *this] --- 龟行协议类对象指针
// 返回: [void] --- 无
// 作者: 耿楠
// 日期: 2022年11月04日
//-----------------------------------------------------------------------*/
void turtle_draw_turtle(void *this)
{
    /* 将指针转换为龟行系统指针 */
    turtle* This = (turtle*)this;

    turtle_t original_turtle = This->main_turtle;

    turtle_pen_up(this);

    /* 画腿 */
    for (int i = -1; i < 2; i += 2)
    {
        for (int j = -1; j < 2; j += 2)
        {
            turtle_push(this);
            turtle_forward(this, i * 4);
            turtle_strafe_left(this, j * 4);

            turtle_set_fill_color_rgb(this,
                    This->main_turtle.fill_color.red,
                    This->main_turtle.fill_color.green,
                    This->main_turtle.fill_color.blue
                    );
            _turtle_fill_circle_here(This, 2);

            turtle_set_fill_color_rgb(this,
                    original_turtle.fill_color.red,
                    original_turtle.fill_color.green,
                    original_turtle.fill_color.blue
                    );
            _turtle_fill_circle_here(This, 2);
            turtle_pop(this);
        }
    }

    /* 画头 */
    turtle_push(this);
    turtle_forward(this, 5);
    turtle_set_fill_color_rgb(this,
            This->main_turtle.fill_color.red,
            This->main_turtle.fill_color.green,
            This->main_turtle.fill_color.blue
            );
    _turtle_fill_circle_here(This, 2);

    turtle_set_fill_color_rgb(this,
            original_turtle.fill_color.red,
            original_turtle.fill_color.green,
            original_turtle.fill_color.blue
            );
    _turtle_fill_circle_here(This, 0.4);
    turtle_pop(this);

    /* 画身体 */
    for (int i = 9; i >= 0; i -= 4)
    {
        turtle_push(this);
        turtle_set_fill_color_rgb(this,
                This->main_turtle.fill_color.red,
                This->main_turtle.fill_color.green,
                This->main_turtle.fill_color.blue
                );
        _turtle_fill_circle_here(This, i - 4);

        turtle_set_fill_color_rgb(this,
                original_turtle.fill_color.red,
                original_turtle.fill_color.green,
                original_turtle.fill_color.blue
                );
        _turtle_fill_circle_here(This, i - 8);
        turtle_pop(this);
    }

    /* 恢复状态 */
    This->main_turtle = original_turtle;
}
