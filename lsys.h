/*--------------------------------------------------------------------------------
 * Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
 * All rights reserved.
 *
 * 文件名称：lsys.h
 * 文件标识：见配置管理计划书
 * 摘要：L-System协议类封装头文件。
 *
 * 当前版本：1.0
 * 作者：耿楠
 * 完成日期：2022年11月11日
 *
 * 取代版本：无
 * 原作者：
 * 完成日期：
 ------------------------------------------------------------------------------------*/
#ifndef LSYS_H_INCLUDE
#define LSYS_H_INCLUDE

/* 引入sds字符串库 */
#include "sds.h"

/* 单条规则结构体 */
typedef struct _rule_t
{
    char ch;
    sds  rule;
}rule_t;

/* 规则被替换字符统计结构体 */
typedef struct _rule_count_t
{
    char ch;
    int  start;
    int  total;
}rule_count_t;

/* L-System结构体(协议类) */
typedef struct _lsys_t
{
    /* 属性 */
    sds           axiom;
    size_t        steps;
    sds           lsys;

    size_t        rules_num;
    size_t        rules_alloc;
    rule_t*       rules;
    size_t        rule_idx_total;
    rule_count_t* rule_count;

    int           is_random;


    /* 初始化系统(指定公理和是否随机) */
    void (*init)(void *, const char *, int);

    /* 设置替换规则(指定被替换字符集和对应的规则--变长形参实现) */
    void (*init_rules)(void *, const char *, ...);
    void (*setrules)(void *, const char *, ...);

    /* 追加一条替换规则(指定一个替换字符和对应的规则) */
    void (*app_rule)(void *, char, const char *);
    void (*app)(void *, char, const char *);

    /* 生成结果字符串，并返回结果字符串指针 */
    sds (*gen_lsys)(void *);
    sds (*gen)(void *);

    /* 返回结果字符串指针 */
    sds (*get_lsys)(void *);

    /* 设置公理 */
    void (*set_axiom)(void *, const char *);
    /* 设置迭代次数 */
    void (*set_steps)(void *, size_t);
    /* 设置是否为随机L-System */
    void (*set_random)(void *, int);

    /* 输出各成员(方便调试) */
    void (*print)(void *);
}lsys_t;

lsys_t * lsys_ctor(lsys_t* this);
void lsys_dtor(lsys_t* this);


#endif
